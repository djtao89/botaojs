"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _proForm = require("@ant-design/pro-form");

var _icons = require("@ant-design/icons");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(props) {
  var subTitle = props.subTitle,
      logo = props.logo,
      backgroundImageUrl = props.backgroundImageUrl,
      message = props.message;
  return /*#__PURE__*/_react.default.createElement("div", {
    style: {
      backgroundColor: 'white',
      height: 'calc(100vh - 48px)',
      margin: -24
    }
  }, /*#__PURE__*/_react.default.createElement(_proForm.LoginFormPage, {
    backgroundImageUrl: "https://gw.alipayobjects.com/zos/rmsportal/FfdJeJRQWjEeGTpqgBKj.png",
    logo: "https://github.githubassets.com/images/modules/logos_page/Octocat.png",
    title: "Github",
    subTitle: "\u5168\u7403\u6700\u5927\u540C\u6027\u4EA4\u53CB\u7F51\u7AD9"
  }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_proForm.ProFormText, {
    name: "username",
    fieldProps: {
      size: 'large',
      prefix: /*#__PURE__*/_react.default.createElement(_icons.UserOutlined, {
        className: 'prefixIcon'
      })
    },
    placeholder: '用户名',
    rules: [{
      required: true,
      message: '请输入用户名!'
    }]
  }), /*#__PURE__*/_react.default.createElement(_proForm.ProFormText.Password, {
    name: "password",
    fieldProps: {
      size: 'large',
      prefix: /*#__PURE__*/_react.default.createElement(_icons.LockOutlined, {
        className: 'prefixIcon'
      })
    },
    placeholder: '密码',
    rules: [{
      required: true,
      message: '请输入密码！'
    }]
  }))));
};

exports.default = _default;