import React from 'react';
import { ReactNode, ReactElement } from 'react';
import './index.less';
declare type OverlayFunc = () => React.ReactElement;
declare const SideMenu: ({ menuDataSource, defaultMenuIcon, logo, title, onMenuCollapse, collapse, notificationRender, personalInfoRender, personalInfoCollapseRender, onMenuItemExpand, menuExpandedKeys, onPathChange, dropdownMenu, defaultSelectedKey, path, ...props }: {
    /**
     * 菜单数据
     */
    menuDataSource: any[];
    /**
     *  菜单默认按钮
     */
    defaultMenuIcon: string;
    /**
     * Header图标
     */
    logo: ReactNode;
    /**
     * Header 标题
     */
    title: ReactNode;
    /**
     * 通知中心渲染
     */
    notificationRender: ReactNode;
    /**
     * 个人中心渲染
     */
    personalInfoRender: ReactNode;
    /**
     * 个人中心收起zhuangt渲染
     */
    personalInfoCollapseRender: ReactNode;
    /**
     *
     * 菜单面板是否收起
     */
    collapse?: boolean | undefined;
    /**
     *  菜单面板收起展开回调
     */
    onMenuCollapse?: ((collapse: boolean) => void) | undefined;
    /**
     * 菜单节点收起展开回调
     *
     */
    onMenuItemExpand?: ((expanded: any[]) => void) | undefined;
    /**
     *
     * 展开的key
     */
    menuExpandedKeys?: string[] | undefined;
    /**
     * 页面切换事件, 用户自行处理跳转事件
     *
     */
    onPathChange: (path: string) => void;
    /**
     *
     * DropDown overlay
     */
    dropdownMenu?: React.ReactElement<any, string | React.JSXElementConstructor<any>> | OverlayFunc | undefined;
    /**
     *
     * 初始化传入的key
     */
    defaultSelectedKey?: any[] | undefined;
    /**
     * 路由参数，改变的话将触发selectKey改变
     */
    path?: string | number | undefined;
}) => JSX.Element;
export default SideMenu;
