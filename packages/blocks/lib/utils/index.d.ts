declare class ChineseInitialsTranslator {
    private static _SpecialCharactors;
    private static _MarkCharactors;
    /**
     * 获取多音字词组
     */
    static get SpecialCharactors(): Record<string, string>;
    /**
     * 获取中文标点符号
     */
    static get MarkCharactors(): string[];
    /**
     * 检测字符串内容是否包含中文字符
     * @param pcText 待检测的字符串
     * @returns 返回检测结果
     */
    static ContainsChinese(pcText: string): boolean;
    /**
     * 检测字符串内容是否包含中文字符
     * @param pcChar 待检测的字符
     * @returns 返回检测结果
     */
    static ContainsChineseChar(pcChar: string): boolean;
    /**
     * 替换特殊字符
     * @param pcText 待检测的字符
     * @returns
     */
    static ReplaceSpecialCharactor(pcText: string): string;
    /**
     * 将中文字符串转译为中文拼音首字母字符串
     * @param pcText 待检测的字符
     * @returns 返回字母字符，中文将被替换为拼音首字母
     */
    static TranslateChineseInitials(pcText: string): string;
}
export default ChineseInitialsTranslator;
