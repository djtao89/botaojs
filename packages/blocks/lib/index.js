"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "LoginPage", {
  enumerable: true,
  get: function get() {
    return _LoginPage.default;
  }
});
Object.defineProperty(exports, "SideMenu", {
  enumerable: true,
  get: function get() {
    return _SideMenu.default;
  }
});

var _SideMenu = _interopRequireDefault(require("./components/SideMenu/"));

var _LoginPage = _interopRequireDefault(require("./components/LoginPage/"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }