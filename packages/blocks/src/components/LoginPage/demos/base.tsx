import React from 'react';
import { LoginPage } from '@d-botao/blocks';

export default () => (
  <>
    {/* login */}
    <LoginPage />
  </>
);
