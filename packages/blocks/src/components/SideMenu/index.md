---
order: 4
title: 侧边菜单
group:
  title: 其他
  path: /other
nav:
  title: 代码块
  path: /blocks
---

### 基本使用

<code src="./demos/base.tsx"  title="基础用法"/>

<API>
