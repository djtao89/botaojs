import Mock from 'mockjs';

export default Mock.mock({
  data: {
    'children|50': [
      {
        'id|+1': 1,
        // icon: 'close',
        funName: '@cword(5,9)',
        path: '/@word(10,15)',
        // leafs:false,
        'children|10': [
          {
            'id|+1': 100,
            funName: '@cword(5,9)',
            // path:"/@word(10,15)",
            // leafs:false,
            'children|20': [
              {
                'id|+1': 10000,
                funName: '@cword(10,12)',
                path: '/@word(10,15)',
                // leafs:true,
              },
            ],
          },
        ],
      },
    ],
  },
});
