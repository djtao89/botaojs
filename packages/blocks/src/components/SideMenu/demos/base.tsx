import React, { useState, useRef } from 'react';
import { Button } from 'antd';
import { SideMenu } from '@d-botao/blocks';
// import ProLayout, { PageContainer } from '@ant-design/pro-layout';
import defaultProps from './data';
// import { useHistory } from 'umi';
import { AlipayCircleOutlined } from '@ant-design/icons';

export default function Base() {
  // const [menuExpandedInfo, setMenuExpandedInfo] = useLocalStorageState('menuExpandedInfo', {
  //   defaultValue: [] as any[],
  // });
  // const history = useHistory();

  // console.log('data', defaultProps);

  const [key, setKey] = useState(10002);

  const ref = useRef(null);

  return (
    <>
      <div
        style={{
          height: '100vh',
        }}
      >
        <Button
          onClick={() => {
            setKey(key + 1);
          }}
        >
          change
        </Button>
        <SideMenu
          logo={<AlipayCircleOutlined style={{ fontSize: '48px', color: '#fff' }} />}
          title="运营风险管控平台"
          menuDataSource={defaultProps.data.children}
          defaultMenuIcon={'chart'}
          onMenuCollapse={(collapse: boolean) => {
            console.log('onMenuCollapse', collapse);
          }}
          onMenuItemExpand={(keys: any[]) => {
            // setMenuExpandedInfo(keys);
          }}
          // menuExpandedKeys={menuExpandedInfo}
          onPathChange={(path: string) => {
            // history.push(path);
          }}
          defaultSelectedKey={[key]}
          path={key}
          // {...menuProps}
        />
      </div>
    </>
  );
}
