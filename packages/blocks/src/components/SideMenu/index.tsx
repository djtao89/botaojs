import React, { useState, useEffect, useMemo, useRef, useLayoutEffect, forwardRef } from 'react';
import { ReactNode, ReactElement, CSSProperties } from 'react';
import { Input, Tree, Dropdown } from 'antd';
import { SearchOutlined, MenuOutlined, DownOutlined, UpOutlined } from '@ant-design/icons';
import { useDebounce, useSize } from 'ahooks';
import ChineseInitialsTranslator from '../../utils/ChineseInitialsTranslator';

import './index.less';

type OverlayFunc = () => React.ReactElement;

// 判断输入字符是否包含中文
const isChinese = (text: string): boolean => {
  const pattern = new RegExp('[\u4e00-\u9fa5]+');
  return pattern.test(text);
};

// 节点路径查找
const treeFindPath = (tree: any[], func: any, path: string[] = []): string[] => {
  if (!tree) return [];
  for (const data of tree) {
    path.push(data.id);
    if (func(data)) return path;
    if (data.children) {
      const findChildren = treeFindPath(data.children, func, path);
      if (findChildren.length) return findChildren;
    }
    path.pop();
  }
  return [];
};

const filterTreeData = (array: any[], text: string): any => {
  const condition = (value: string, target: string): boolean => {
    if (isChinese(value)) {
      return target.indexOf(value) > -1;
    } else {
      return (
        ChineseInitialsTranslator.TranslateChineseInitials(target)
          .toLocaleUpperCase()
          .indexOf(value.toLocaleUpperCase().trim()) > -1 ||
        target.indexOf(value.toLocaleUpperCase().trim()) > -1
      );
    }
  };
  const filterKeys: string[] = [];
  const getNodes = (result: any[], object: any) => {
    if (condition(text, object.funName)) {
      filterKeys.push(object.id);
      result.push(object);
      return result;
    }
    if (Array.isArray(object.children)) {
      const children = object.children.reduce(getNodes, []);
      if (children.length) {
        result.push({ ...object, children });
      }
    }
    return result;
  };

  return { filter: array.reduce(getNodes, []), filterKeys };
};

/**
 * @description 获取树形所有孩子节点
 * @title 获取树形所有孩子节点
 * @export []
 */
function getAllChildrens(nodes: any[]) {
  const childNodes: any[] = [];
  nodes.forEach((item: any) => {
    if (item.children?.length) {
      childNodes.push(...getAllChildrens(item.children));
    } else {
      childNodes.push(item);
    }
  });
  return childNodes;
}

// const filterTreeData = (array: any[], text: string): any => {
//   const condition = (value: string, target: string): boolean => {
//     if (isChinese(value)) {
//       return target.indexOf(value) > -1;
//     } else {
//       return (
//         ChineseInitialsTranslator.TranslateChineseInitials(target)
//           .toLocaleUpperCase()
//           .indexOf(value.toLocaleUpperCase().trim()) > -1 ||
//         target.indexOf(value.toLocaleUpperCase().trim()) > -1
//       );
//     }
//   };
//   const filterKeys: string[] = [];
//   const getNodes = (result: any[], object: any) => {
//     if (condition(text, object.funName)) {
//       filterKeys.push(object.id);
//       result.push(object);
//       return result;
//     }
//     if (Array.isArray(object.children)) {
//       const children = object.children.reduce(getNodes, []);
//       if (children.length) {
//         result.push({ ...object, children });
//       }
//     }
//     return result;
//   };

//   return { filter: array.reduce(getNodes, []), filterKeys };
// };

// 判断是否是数组
const isValidArray = (value: any[]) => value && Array.isArray(value);

//  转换为 Tree 要求的数据结构
const convertData = (data: any[], defaultIcon?: any): any[] => {
  return data.map((node: any) => {
    return {
      key: node.id,
      title: node.funName,
      icon: node.icon || defaultIcon,
      path: node.path,
      children: node.children && node.children.length > 0 && convertData(node.children),
    };
  });
};

const SideMenu = ({
  menuDataSource,
  defaultMenuIcon,
  logo,
  title,
  onMenuCollapse = () => {},
  collapse = false,
  notificationRender = null,
  personalInfoRender = null,
  personalInfoCollapseRender,
  onMenuItemExpand = () => {},
  menuExpandedKeys,
  onPathChange,
  dropdownMenu,
  defaultSelectedKey = [],
  path = '',
  ...props
}: {
  /**
   * 菜单数据
   */
  menuDataSource: any[];

  /**
   *  菜单默认按钮
   */
  defaultMenuIcon: string;
  /**
   * Header图标
   */
  logo: ReactNode;
  /**
   * Header 标题
   */
  title: ReactNode;

  /**
   * 通知中心渲染
   */
  notificationRender: ReactNode;

  /**
   * 个人中心渲染
   */
  personalInfoRender: ReactNode;

  /**
   * 个人中心收起zhuangt渲染
   */
  personalInfoCollapseRender: ReactNode;

  /**
   *
   * 菜单面板是否收起
   */
  collapse?: boolean;

  /**
   *  菜单面板收起展开回调
   */
  onMenuCollapse?: (collapse: boolean) => void;

  /**
   * 菜单节点收起展开回调
   *
   */
  onMenuItemExpand?: (expanded: any[]) => void;

  /**
   *
   * 展开的key
   */
  menuExpandedKeys?: string[] | undefined;

  /**
   * 页面切换事件, 用户自行处理跳转事件
   *
   */
  onPathChange: (path: string) => void;

  /**
   *
   * DropDown overlay
   */
  dropdownMenu?: React.ReactElement | OverlayFunc;
  /**
   *
   * 初始化传入的key
   */
  defaultSelectedKey?: any[];
  /**
   * 路由参数，改变的话将触发selectKey改变
   */
  path?: string | number;
}) => {
  const siderbarMenuRef = useRef(null as HTMLDivElement | any);

  const [menuCollapse, setMenuCollapse] = useState(collapse);
  // 搜索关键字
  const [keyWord, setKeyWord] = useState('');
  const [currentMenuDataSource, setCurrentMenuDataSource] = useState(menuDataSource);

  const [treeHeight, setTreeHeight] = useState(300);

  // 默认选中表现处理
  const [selectedKey, setSelectedKey] = useState(defaultSelectedKey || []);
  const parent = treeFindPath(
    currentMenuDataSource,
    (node: any) => node.id === defaultSelectedKey[0],
  ).filter((key: string) => key !== defaultSelectedKey[0]);
  const [selectedParentKeys, setSelectedParentKeys] = useState<string[]>(parent);
  // const [expandedKeys, setExpandedKeys] = useState(menuExpandedKeys);
  const [expandedKeys, setExpandedKeys] = useState<any>([
    ...parent,
    ...defaultSelectedKey,
    ...(menuExpandedKeys || []),
  ]);

  // 所有叶子节点的key，用于控制选中样式
  const childKeys = getAllChildrens(currentMenuDataSource).map((itme: any) => itme.id);

  const debouncedKeyWord = useDebounce(keyWord, { wait: 500 });

  // 头部
  const menuHeader = (collapsed: boolean) => {
    return (
      <div className="menuHeader">
        <a href="/" className="menuLogo">
          {logo}
          {collapsed ? null : <span className="headerTitle">{title}</span>}
        </a>
      </div>
    );
  };

  // 菜单搜索
  const menuSearchRender = () => {
    return (
      <div className="menuSearch">
        <Input
          size="large"
          placeholder="请输入关键词进行搜索"
          prefix={
            <SearchOutlined
              className="searchIcon"
              style={{ fontSize: '18px', marginLeft: '16px', color: '#d8d8d8' }}
            />
          }
          bordered={false}
          maxLength={50}
          allowClear
          style={{
            width: '239px',
            height: '48px',
            opacity: 1,
            background: '#1a1f32',
          }}
          onChange={(e: any) => setKeyWord(e.target.value.trim())}
        />
      </div>
    );
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const convertDataWithMemo = useMemo(
    () => convertData(currentMenuDataSource, defaultMenuIcon),
    [currentMenuDataSource, defaultMenuIcon],
  );

  /**
   *
   * 节点展开收起回调
   */
  const onExpand = (keys: any[]) => {
    onMenuItemExpand(keys);
    setExpandedKeys(keys);
  };

  /**
   * 菜单节点渲染
   *
   * @param {*} node
   * @return {*}
   */
  const renderMenuItem = (node: any) => {
    const { key }: { key: string } = node;
    const publicStyle: CSSProperties = {
      marginLeft: '18px',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      width: '126px',
      whiteSpace: 'nowrap',
      color: selectedParentKeys.includes(key)
        ? 'rgba(255, 255, 255, 1)'
        : 'rgba(255, 255, 255, 0.6)',
    };
    return (
      <div
        className="menuItem"
        onClick={() => !isValidArray(node.children) && onPathChange(node.path)}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          {node.icon && <MenuOutlined style={{ fontSize: '20px', marginLeft: '20px' }} />}
          {node.path ? (
            <span style={publicStyle}>{node.title}</span>
          ) : (
            <>
              <span style={publicStyle}>{node.title}</span>
              {!isValidArray(node.children) && (
                <MenuOutlined
                  type="forbidden-o"
                  style={{ fontSize: '14px', marginLeft: '10px', color: 'red', opacity: '0.5' }}
                />
              )}
            </>
          )}
        </div>

        {isValidArray(node.children) && (
          <DownOutlined
            // , UpOutlined
            className="anticon-down"
            style={{ marginRight: '15px', fontSize: '13px' }}
          />
        )}
      </div>
    );
  };

  // const [key, setKeys] = useState(0);

  useEffect(() => {
    // setKeys(Date.now());
    const selectedKeys = [path];
    setSelectedKey(selectedKeys);
    const parent = treeFindPath(
      currentMenuDataSource,
      (node: any) => node.id === selectedKeys[0],
    ).filter((key: string) => key !== selectedKeys[0]);

    setSelectedParentKeys(parent);
    setExpandedKeys(Array.from(new Set([...parent, ...(expandedKeys ? expandedKeys : [])])));
  }, [path]);

  /**
   * 左侧菜单渲染
   * @param {any[]} dataSource
   * @return {*}
   */
  const siderbarMenu = () => {
    const emptyBox = <div />;
    return (
      <div className="menuContent" ref={siderbarMenuRef}>
        <Tree.DirectoryTree
          // autoExpandParent={autoExpandAll}
          treeData={convertDataWithMemo}
          titleRender={(node: any) => {
            return renderMenuItem(node);
          }}
          onExpand={onExpand}
          expandedKeys={expandedKeys}
          onSelect={(selectedKeys: any, e: any) => {
            if (childKeys.includes(selectedKeys[0])) {
              setSelectedKey(selectedKeys);
              const parent = treeFindPath(
                currentMenuDataSource,
                (node: any) => node.id === selectedKeys[0],
              ).filter((key: string) => key !== selectedKeys[0]);
              setSelectedParentKeys(parent);
            }
          }}
          selectedKeys={(() => {
            return selectedKey;
          })()}
          switcherIcon={emptyBox}
          showIcon={false}
          blockNode
          style={{ backgroundColor: '#262D48' }}
          height={treeHeight}
        />
      </div>
    );
  };

  /**
   * 菜单展开底部内容
   *
   * @param {string} name
   * @return {*}
   */
  const menuFooterPersonalRender = (personalInfoDom: ReactNode, notificationDom: ReactNode) => {
    return (
      <div className="menuFooterPersonal">
        <div className="menuFooterName">{personalInfoDom}</div>

        <div className="menuFooterInformation">{notificationDom}</div>
      </div>
    );
  };

  const menuFooterCollapsePersonalRender = (
    personalInfoCollapseDom: ReactNode,
    notificationDom: ReactNode,
  ) => {
    return (
      <div className="menuFooterCollapsePersonal">
        <div style={{ width: '56px', height: '56px', lineHeight: '56px', textAlign: 'center' }}>
          {notificationDom}
        </div>
        <div
          style={{
            width: '56px',
            height: '56px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {personalInfoCollapseDom}
        </div>
      </div>
    );
  };

  /**
   * 收起菜单栏
   * @param {any[]} dataSource
   * @return {*}
   */
  const siderbarCollapseMenu = (dataSource: any[], defaultIcon: string) => {
    return (
      isValidArray(dataSource) && (
        <div className="collapsedMenuContent">
          {dataSource.map((node: any) => {
            return (
              <>ClosedSider</>
              // <ClosedSider
              //   key={`${node.id}_${node.path}`}
              //   routes={node.children}
              //   name={node.funName}
              //   allRoutes={dataSource}
              // >
              //   <div
              //     className={
              //       node.id === selectedParentKeys[0]
              //         ? 'collapsedMenuIconWrapper collapsedMenuIconWrapperSelected'
              //         : 'collapsedMenuIconWrapper'
              //     }
              //   >
              //     <QuarkIcon type={node.icon || defaultIcon} style={{ fontSize: '20px' }} />
              //   </div>
              // </ClosedSider>
            );
          })}
        </div>
      )
    );
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const collapseMenu = useMemo(
    () => siderbarCollapseMenu(currentMenuDataSource, defaultMenuIcon),
    [currentMenuDataSource, defaultMenuIcon, selectedParentKeys],
  );
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const defaultMenu = useMemo(
    () => siderbarMenu(),
    [currentMenuDataSource, expandedKeys, treeHeight, path, selectedParentKeys],
  );

  // 监听Menu区域高度动态设置tree高度
  const contonerHeight: any = useSize(siderbarMenuRef)?.height;
  const contonerDebounHeight: any = useDebounce(contonerHeight, { wait: 200 });
  useLayoutEffect(() => {
    // DOM 更新完成后打印出 div 的高度
    // console.log("contonerDebounHeight",contonerDebounHeight);

    if (!menuCollapse) {
      setTreeHeight(contonerDebounHeight);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [menuCollapse, contonerDebounHeight]);

  // 搜索功能
  useEffect(() => {
    if (debouncedKeyWord.length > 0) {
      const { filter, filterKeys } = filterTreeData(menuDataSource, debouncedKeyWord);
      setCurrentMenuDataSource(filter);
      let keys: string[] = [];
      filterKeys.forEach((key: any) => {
        const path = treeFindPath(filter, (node: any) => node.id === key);
        if (path.length > 0) {
          keys = keys.concat(path);
        }
      });

      setExpandedKeys([...new Set(keys)]);
      // setAutoExpandAll(true);
    }
    if (debouncedKeyWord.length === 0) {
      // 数据初始化
      setCurrentMenuDataSource(menuDataSource);
      setExpandedKeys(menuExpandedKeys);
      // setAutoExpandAll(false);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedKeyWord]);

  // 收缩展开
  useEffect(() => {
    setMenuCollapse(collapse);
  }, [collapse]);

  return (
    <>
      <div className="menuWrapper" style={{ width: menuCollapse ? '56px' : '240px' }}>
        {menuHeader(menuCollapse)}
        {menuCollapse ? null : menuSearchRender()}
        {dropdownMenu ? (
          <Dropdown overlay={dropdownMenu} trigger={['contextMenu']}>
            {menuCollapse ? collapseMenu : defaultMenu}
          </Dropdown>
        ) : (
          <>{menuCollapse ? collapseMenu : defaultMenu}</>
        )}
        <div className="menuFooter" style={{ height: menuCollapse ? '120px' : '56px' }}>
          {menuCollapse &&
            menuFooterCollapsePersonalRender(personalInfoCollapseRender, notificationRender)}
          {!menuCollapse && menuFooterPersonalRender(personalInfoRender, notificationRender)}
        </div>
      </div>
    </>
  );
};

export default SideMenu;
