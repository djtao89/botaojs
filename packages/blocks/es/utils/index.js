function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/* eslint-disable no-underscore-dangle */
var ChineseInitialsTranslator = /*#__PURE__*/function () {
  function ChineseInitialsTranslator() {
    _classCallCheck(this, ChineseInitialsTranslator);
  }

  _createClass(ChineseInitialsTranslator, null, [{
    key: "SpecialCharactors",
    get:
    /**
     * 获取多音字词组
     */
    function get() {
      var loTempDict = ChineseInitialsTranslator._SpecialCharactors;

      if (loTempDict === undefined) {
        loTempDict = {
          重叠: 'cd',
          重复: 'cf',
          重分类: 'cfl',
          重庆: 'cq',
          成长: 'cz',
          东莞: 'dg',
          工行: 'gh',
          大厦: 'ds',
          广厦: 'gs',
          行情: 'hq',
          行业: 'hy',
          建行: 'jh',
          会计: 'kj',
          农行: 'nh',
          商厦: 'ss',
          调低: 'td',
          调高: 'tg',
          调节: 'tj',
          调解: 'tj',
          调配: 'tp',
          调试: 'ts',
          调尾: 'tw',
          调整: 'tz',
          西藏: 'xz',
          银行: 'yh',
          中行: 'zh',
          藏南: 'zn',
          霭: 'a',
          璧: 'b',
          汴: 'b',
          亳: 'b',
          漕: 'c',
          昶: 'c',
          晁: 'c',
          萃: 'c',
          棰: 'c',
          岱: 'd',
          黛: 'd',
          笃: 'd',
          鳄: 'e',
          梵: 'f',
          斐: 'f',
          翡: 'f',
          妃: 'f',
          沣: 'f',
          芙: 'f',
          孚: 'f',
          馥: 'f',
          伽: 'g',
          锆: 'g',
          罡: 'g',
          亘: 'g',
          枸: 'g',
          卦: 'g',
          莞: 'g',
          嗨: 'h',
          晗: 'h',
          瀚: 'h',
          昊: 'h',
          颢: 'h',
          皓: 'h',
          濠: 'h',
          泓: 'h',
          琥: 'h',
          桦: 'h',
          寰: 'h',
          晖: 'h',
          睢: 'h',
          荟: 'h',
          玑: 'j',
          骥: 'j',
          迦: 'j',
          珈: 'j',
          桀: 'j',
          泾: 'j',
          菁: 'j',
          旌: 'j',
          鸠: 'j',
          飓: 'j',
          隽: 'j',
          珏: 'j',
          铠: 'k',
          恺: 'k',
          珂: 'k',
          轲: 'k',
          稞: 'k',
          徕: 'l',
          岚: 'l',
          螂: 'l',
          耒: 'l',
          锂: 'l',
          翎: 'l',
          珑: 'l',
          浏: 'l',
          砻: 'l',
          鹭: 'l',
          泸: 'l',
          榈: 'l',
          蟒: 'm',
          淼: 'm',
          茗: 'm',
          湄: 'm',
          闵: 'm',
          岷: 'm',
          钼: 'm',
          沐: 'm',
          楠: 'n',
          鲵: 'n',
          瓯: 'o',
          邳: 'p',
          珀: 'p',
          琪: 'q',
          琦: 'q',
          祺: 'q',
          杞: 'q',
          蜻: 'q',
          裘: 'q',
          衢: 'q',
          荃: 'q',
          榕: 'r',
          睿: 'r',
          熵: 's',
          笙: 's',
          晟: 's',
          俬: 's',
          泗: 's',
          狩: 's',
          沭: 's',
          他: 't',
          钛: 't',
          螳: 't',
          韬: 't',
          滕: 't',
          婷: 't',
          蜓: 't',
          沱: 't',
          娲: 'w',
          帷: 'w',
          圩: 'w',
          汶: 'w',
          喔: 'w',
          熹: 'x',
          禧: 'x',
          奚: 'x',
          曦: 'x',
          玺: 'x',
          呷: 'x',
          籼: 'x',
          岘: 'x',
          榭: 'x',
          鑫: 'x',
          馨: 'x',
          莘: 'x',
          昕: 'x',
          煦: 'x',
          萱: 'x',
          炫: 'x',
          浔: 'x',
          兖: 'y',
          晏: 'y',
          炀: 'y',
          鹞: 'y',
          曜: 'y',
          晔: 'y',
          怡: 'y',
          懿: 'y',
          羿: 'y',
          弈: 'y',
          奕: 'y',
          熠: 'y',
          翊: 'y',
          颍: 'y',
          楹: 'y',
          甬: 'y',
          钰: 'y',
          昱: 'y',
          煜: 'y',
          瑜: 'y',
          芸: 'y',
          獐: 'z',
          钊: 'z',
          柘: 'z',
          梓: 'z',
          祯: 'z',
          圳: 'z',
          竺: 'z',
          渚: 'z',
          骓: 'z'
        };
        ChineseInitialsTranslator._SpecialCharactors = loTempDict;
      }

      return loTempDict;
    }
    /**
     * 获取中文标点符号
     */

  }, {
    key: "MarkCharactors",
    get: function get() {
      var loTempMarks = ChineseInitialsTranslator._MarkCharactors;

      if (loTempMarks === undefined) {
        loTempMarks = ['，', '。', '？', '《', '》', '、', '；', '：', '“', '”', '‘', '’', '·', '…', '￥', '（', '）', '【', '】', '{', '}', '！'];
        ChineseInitialsTranslator._MarkCharactors = loTempMarks;
      }

      return loTempMarks;
    }
    /**
     * 检测字符串内容是否包含中文字符
     * @param pcText 待检测的字符串
     * @returns 返回检测结果
     */

  }, {
    key: "ContainsChinese",
    value: function ContainsChinese(pcText) {
      var llState = false;

      if (escape(pcText).indexOf('%u') >= 0) {
        llState = true;
      }

      return llState;
    }
    /**
     * 检测字符串内容是否包含中文字符
     * @param pcChar 待检测的字符
     * @returns 返回检测结果
     */

  }, {
    key: "ContainsChineseChar",
    value: function ContainsChineseChar(pcChar) {
      var llState = false;
      var loMarks = ChineseInitialsTranslator.MarkCharactors;

      if (loMarks.indexOf(pcChar) > -1) {
        return llState;
      }

      if (escape(pcChar).indexOf('%u') >= 0) {
        llState = true;
      }

      return llState;
    }
    /**
     * 替换特殊字符
     * @param pcText 待检测的字符
     * @returns
     */

  }, {
    key: "ReplaceSpecialCharactor",
    value: function ReplaceSpecialCharactor(pcText) {
      var lcText = pcText;
      var loSpecialCharactors = ChineseInitialsTranslator.SpecialCharactors;
      var loAllKeys = Object.keys(loSpecialCharactors);
      loAllKeys.forEach(function (lcTempKey) {
        lcText = lcText.replaceAll(lcTempKey, loSpecialCharactors[lcTempKey]);
      });
      return lcText;
    }
    /**
     * 将中文字符串转译为中文拼音首字母字符串
     * @param pcText 待检测的字符
     * @returns 返回字母字符，中文将被替换为拼音首字母
     */

  }, {
    key: "TranslateChineseInitials",
    value: function TranslateChineseInitials(pcText) {
      var lcResult = '';

      if (pcText === undefined || pcText === null) {
        return lcResult;
      }

      var lcText = String(pcText);
      lcText = ChineseInitialsTranslator.ReplaceSpecialCharactor(lcText);
      var loLetterArray = 'abcdefghjklmnopqrstwxyz'.split('');
      var loCharArray = '阿八嚓哒妸发旮哈讥咔垃妈拏噢妑七呥撒它穵夕丫帀'.split('');
      var loChineseArray = lcText.split('');

      for (var liIndex = 0; liIndex < loChineseArray.length; liIndex += 1) {
        var lcChineseChar = loChineseArray[liIndex];

        if (ChineseInitialsTranslator.ContainsChineseChar(lcChineseChar)) {
          for (var liLetterIndex = 0; liLetterIndex < loLetterArray.length; liLetterIndex += 1) {
            var lcLetterChar = loLetterArray[liLetterIndex];
            var lcTempCharBegin = loCharArray[liLetterIndex];
            var lcTempCharEnd = loCharArray[liLetterIndex + 1];

            if (lcChineseChar.localeCompare(lcTempCharBegin, 'zh-Hans-CN') >= 0 && lcChineseChar.localeCompare(lcTempCharEnd, 'zh-Hans-CN') < 0) {
              lcResult += lcLetterChar;
            }
          }
        } else {
          lcResult += lcChineseChar;
        }
      }

      return lcResult;
    }
  }]);

  return ChineseInitialsTranslator;
}();

ChineseInitialsTranslator._SpecialCharactors = void 0;
ChineseInitialsTranslator._MarkCharactors = void 0;
export default ChineseInitialsTranslator;