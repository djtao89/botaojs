import React from 'react';
import { LoginFormPage, ProFormText } from '@ant-design/pro-form';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
export default (function (props) {
  var subTitle = props.subTitle,
      logo = props.logo,
      backgroundImageUrl = props.backgroundImageUrl,
      message = props.message;
  return /*#__PURE__*/React.createElement("div", {
    style: {
      backgroundColor: 'white',
      height: 'calc(100vh - 48px)',
      margin: -24
    }
  }, /*#__PURE__*/React.createElement(LoginFormPage, {
    backgroundImageUrl: "https://gw.alipayobjects.com/zos/rmsportal/FfdJeJRQWjEeGTpqgBKj.png",
    logo: "https://github.githubassets.com/images/modules/logos_page/Octocat.png",
    title: "Github",
    subTitle: "\u5168\u7403\u6700\u5927\u540C\u6027\u4EA4\u53CB\u7F51\u7AD9"
  }, /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ProFormText, {
    name: "username",
    fieldProps: {
      size: 'large',
      prefix: /*#__PURE__*/React.createElement(UserOutlined, {
        className: 'prefixIcon'
      })
    },
    placeholder: '用户名',
    rules: [{
      required: true,
      message: '请输入用户名!'
    }]
  }), /*#__PURE__*/React.createElement(ProFormText.Password, {
    name: "password",
    fieldProps: {
      size: 'large',
      prefix: /*#__PURE__*/React.createElement(LockOutlined, {
        className: 'prefixIcon'
      })
    },
    placeholder: '密码',
    rules: [{
      required: true,
      message: '请输入密码！'
    }]
  }))));
});