import "antd/es/dropdown/style";
import _Dropdown from "antd/es/dropdown";
import "antd/es/tree/style";
import _Tree from "antd/es/tree";
import "antd/es/input/style";
import _Input from "antd/es/input";
var _excluded = ["menuDataSource", "defaultMenuIcon", "logo", "title", "onMenuCollapse", "collapse", "notificationRender", "personalInfoRender", "personalInfoCollapseRender", "onMenuItemExpand", "menuExpandedKeys", "onPathChange", "dropdownMenu", "defaultSelectedKey", "path"];

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

import React, { useState, useEffect, useMemo, useRef, useLayoutEffect } from 'react';
import { SearchOutlined, MenuOutlined, DownOutlined } from '@ant-design/icons';
import { useDebounce, useSize } from 'ahooks';
import ChineseInitialsTranslator from '../../utils/ChineseInitialsTranslator';
import './index.less'; // 判断输入字符是否包含中文

var isChinese = function isChinese(text) {
  var pattern = new RegExp("[\u4E00-\u9FA5]+");
  return pattern.test(text);
}; // 节点路径查找


var treeFindPath = function treeFindPath(tree, func) {
  var path = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  if (!tree) return [];

  var _iterator = _createForOfIteratorHelper(tree),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var data = _step.value;
      path.push(data.id);
      if (func(data)) return path;

      if (data.children) {
        var findChildren = treeFindPath(data.children, func, path);
        if (findChildren.length) return findChildren;
      }

      path.pop();
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return [];
};

var filterTreeData = function filterTreeData(array, text) {
  var condition = function condition(value, target) {
    if (isChinese(value)) {
      return target.indexOf(value) > -1;
    } else {
      return ChineseInitialsTranslator.TranslateChineseInitials(target).toLocaleUpperCase().indexOf(value.toLocaleUpperCase().trim()) > -1 || target.indexOf(value.toLocaleUpperCase().trim()) > -1;
    }
  };

  var filterKeys = [];

  var getNodes = function getNodes(result, object) {
    if (condition(text, object.funName)) {
      filterKeys.push(object.id);
      result.push(object);
      return result;
    }

    if (Array.isArray(object.children)) {
      var children = object.children.reduce(getNodes, []);

      if (children.length) {
        result.push(_objectSpread(_objectSpread({}, object), {}, {
          children: children
        }));
      }
    }

    return result;
  };

  return {
    filter: array.reduce(getNodes, []),
    filterKeys: filterKeys
  };
};
/**
 * @description 获取树形所有孩子节点
 * @title 获取树形所有孩子节点
 * @export []
 */


function getAllChildrens(nodes) {
  var childNodes = [];
  nodes.forEach(function (item) {
    var _item$children;

    if ((_item$children = item.children) === null || _item$children === void 0 ? void 0 : _item$children.length) {
      childNodes.push.apply(childNodes, _toConsumableArray(getAllChildrens(item.children)));
    } else {
      childNodes.push(item);
    }
  });
  return childNodes;
} // const filterTreeData = (array: any[], text: string): any => {
//   const condition = (value: string, target: string): boolean => {
//     if (isChinese(value)) {
//       return target.indexOf(value) > -1;
//     } else {
//       return (
//         ChineseInitialsTranslator.TranslateChineseInitials(target)
//           .toLocaleUpperCase()
//           .indexOf(value.toLocaleUpperCase().trim()) > -1 ||
//         target.indexOf(value.toLocaleUpperCase().trim()) > -1
//       );
//     }
//   };
//   const filterKeys: string[] = [];
//   const getNodes = (result: any[], object: any) => {
//     if (condition(text, object.funName)) {
//       filterKeys.push(object.id);
//       result.push(object);
//       return result;
//     }
//     if (Array.isArray(object.children)) {
//       const children = object.children.reduce(getNodes, []);
//       if (children.length) {
//         result.push({ ...object, children });
//       }
//     }
//     return result;
//   };
//   return { filter: array.reduce(getNodes, []), filterKeys };
// };
// 判断是否是数组


var isValidArray = function isValidArray(value) {
  return value && Array.isArray(value);
}; //  转换为 Tree 要求的数据结构


var convertData = function convertData(data, defaultIcon) {
  return data.map(function (node) {
    return {
      key: node.id,
      title: node.funName,
      icon: node.icon || defaultIcon,
      path: node.path,
      children: node.children && node.children.length > 0 && convertData(node.children)
    };
  });
};

var SideMenu = function SideMenu(_ref) {
  var _useSize;

  var menuDataSource = _ref.menuDataSource,
      defaultMenuIcon = _ref.defaultMenuIcon,
      logo = _ref.logo,
      title = _ref.title,
      _ref$onMenuCollapse = _ref.onMenuCollapse,
      onMenuCollapse = _ref$onMenuCollapse === void 0 ? function () {} : _ref$onMenuCollapse,
      _ref$collapse = _ref.collapse,
      collapse = _ref$collapse === void 0 ? false : _ref$collapse,
      _ref$notificationRend = _ref.notificationRender,
      notificationRender = _ref$notificationRend === void 0 ? null : _ref$notificationRend,
      _ref$personalInfoRend = _ref.personalInfoRender,
      personalInfoRender = _ref$personalInfoRend === void 0 ? null : _ref$personalInfoRend,
      personalInfoCollapseRender = _ref.personalInfoCollapseRender,
      _ref$onMenuItemExpand = _ref.onMenuItemExpand,
      onMenuItemExpand = _ref$onMenuItemExpand === void 0 ? function () {} : _ref$onMenuItemExpand,
      menuExpandedKeys = _ref.menuExpandedKeys,
      onPathChange = _ref.onPathChange,
      dropdownMenu = _ref.dropdownMenu,
      _ref$defaultSelectedK = _ref.defaultSelectedKey,
      defaultSelectedKey = _ref$defaultSelectedK === void 0 ? [] : _ref$defaultSelectedK,
      _ref$path = _ref.path,
      path = _ref$path === void 0 ? '' : _ref$path,
      props = _objectWithoutProperties(_ref, _excluded);

  var siderbarMenuRef = useRef(null);

  var _useState = useState(collapse),
      _useState2 = _slicedToArray(_useState, 2),
      menuCollapse = _useState2[0],
      setMenuCollapse = _useState2[1]; // 搜索关键字


  var _useState3 = useState(''),
      _useState4 = _slicedToArray(_useState3, 2),
      keyWord = _useState4[0],
      setKeyWord = _useState4[1];

  var _useState5 = useState(menuDataSource),
      _useState6 = _slicedToArray(_useState5, 2),
      currentMenuDataSource = _useState6[0],
      setCurrentMenuDataSource = _useState6[1];

  var _useState7 = useState(300),
      _useState8 = _slicedToArray(_useState7, 2),
      treeHeight = _useState8[0],
      setTreeHeight = _useState8[1]; // 默认选中表现处理


  var _useState9 = useState(defaultSelectedKey || []),
      _useState10 = _slicedToArray(_useState9, 2),
      selectedKey = _useState10[0],
      setSelectedKey = _useState10[1];

  var parent = treeFindPath(currentMenuDataSource, function (node) {
    return node.id === defaultSelectedKey[0];
  }).filter(function (key) {
    return key !== defaultSelectedKey[0];
  });

  var _useState11 = useState(parent),
      _useState12 = _slicedToArray(_useState11, 2),
      selectedParentKeys = _useState12[0],
      setSelectedParentKeys = _useState12[1]; // const [expandedKeys, setExpandedKeys] = useState(menuExpandedKeys);


  var _useState13 = useState([].concat(_toConsumableArray(parent), _toConsumableArray(defaultSelectedKey), _toConsumableArray(menuExpandedKeys || []))),
      _useState14 = _slicedToArray(_useState13, 2),
      expandedKeys = _useState14[0],
      setExpandedKeys = _useState14[1]; // 所有叶子节点的key，用于控制选中样式


  var childKeys = getAllChildrens(currentMenuDataSource).map(function (itme) {
    return itme.id;
  });
  var debouncedKeyWord = useDebounce(keyWord, {
    wait: 500
  }); // 头部

  var menuHeader = function menuHeader(collapsed) {
    return /*#__PURE__*/React.createElement("div", {
      className: "menuHeader"
    }, /*#__PURE__*/React.createElement("a", {
      href: "/",
      className: "menuLogo"
    }, logo, collapsed ? null : /*#__PURE__*/React.createElement("span", {
      className: "headerTitle"
    }, title)));
  }; // 菜单搜索


  var menuSearchRender = function menuSearchRender() {
    return /*#__PURE__*/React.createElement("div", {
      className: "menuSearch"
    }, /*#__PURE__*/React.createElement(_Input, {
      size: "large",
      placeholder: "\u8BF7\u8F93\u5165\u5173\u952E\u8BCD\u8FDB\u884C\u641C\u7D22",
      prefix: /*#__PURE__*/React.createElement(SearchOutlined, {
        className: "searchIcon",
        style: {
          fontSize: '18px',
          marginLeft: '16px',
          color: '#d8d8d8'
        }
      }),
      bordered: false,
      maxLength: 50,
      allowClear: true,
      style: {
        width: '239px',
        height: '48px',
        opacity: 1,
        background: '#1a1f32'
      },
      onChange: function onChange(e) {
        return setKeyWord(e.target.value.trim());
      }
    }));
  }; // eslint-disable-next-line react-hooks/exhaustive-deps


  var convertDataWithMemo = useMemo(function () {
    return convertData(currentMenuDataSource, defaultMenuIcon);
  }, [currentMenuDataSource, defaultMenuIcon]);
  /**
   *
   * 节点展开收起回调
   */

  var onExpand = function onExpand(keys) {
    onMenuItemExpand(keys);
    setExpandedKeys(keys);
  };
  /**
   * 菜单节点渲染
   *
   * @param {*} node
   * @return {*}
   */


  var renderMenuItem = function renderMenuItem(node) {
    var key = node.key;
    var publicStyle = {
      marginLeft: '18px',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      width: '126px',
      whiteSpace: 'nowrap',
      color: selectedParentKeys.includes(key) ? 'rgba(255, 255, 255, 1)' : 'rgba(255, 255, 255, 0.6)'
    };
    return /*#__PURE__*/React.createElement("div", {
      className: "menuItem",
      onClick: function onClick() {
        return !isValidArray(node.children) && onPathChange(node.path);
      }
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
      }
    }, node.icon && /*#__PURE__*/React.createElement(MenuOutlined, {
      style: {
        fontSize: '20px',
        marginLeft: '20px'
      }
    }), node.path ? /*#__PURE__*/React.createElement("span", {
      style: publicStyle
    }, node.title) : /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("span", {
      style: publicStyle
    }, node.title), !isValidArray(node.children) && /*#__PURE__*/React.createElement(MenuOutlined, {
      type: "forbidden-o",
      style: {
        fontSize: '14px',
        marginLeft: '10px',
        color: 'red',
        opacity: '0.5'
      }
    }))), isValidArray(node.children) && /*#__PURE__*/React.createElement(DownOutlined // , UpOutlined
    , {
      // , UpOutlined
      className: "anticon-down",
      style: {
        marginRight: '15px',
        fontSize: '13px'
      }
    }));
  }; // const [key, setKeys] = useState(0);


  useEffect(function () {
    // setKeys(Date.now());
    var selectedKeys = [path];
    setSelectedKey(selectedKeys);
    var parent = treeFindPath(currentMenuDataSource, function (node) {
      return node.id === selectedKeys[0];
    }).filter(function (key) {
      return key !== selectedKeys[0];
    });
    setSelectedParentKeys(parent);
    setExpandedKeys(Array.from(new Set([].concat(_toConsumableArray(parent), _toConsumableArray(expandedKeys ? expandedKeys : [])))));
  }, [path]);
  /**
   * 左侧菜单渲染
   * @param {any[]} dataSource
   * @return {*}
   */

  var siderbarMenu = function siderbarMenu() {
    var emptyBox = /*#__PURE__*/React.createElement("div", null);
    return /*#__PURE__*/React.createElement("div", {
      className: "menuContent",
      ref: siderbarMenuRef
    }, /*#__PURE__*/React.createElement(_Tree.DirectoryTree // autoExpandParent={autoExpandAll}
    , {
      // autoExpandParent={autoExpandAll}
      treeData: convertDataWithMemo,
      titleRender: function titleRender(node) {
        return renderMenuItem(node);
      },
      onExpand: onExpand,
      expandedKeys: expandedKeys,
      onSelect: function onSelect(selectedKeys, e) {
        if (childKeys.includes(selectedKeys[0])) {
          setSelectedKey(selectedKeys);

          var _parent = treeFindPath(currentMenuDataSource, function (node) {
            return node.id === selectedKeys[0];
          }).filter(function (key) {
            return key !== selectedKeys[0];
          });

          setSelectedParentKeys(_parent);
        }
      },
      selectedKeys: function () {
        return selectedKey;
      }(),
      switcherIcon: emptyBox,
      showIcon: false,
      blockNode: true,
      style: {
        backgroundColor: '#262D48'
      },
      height: treeHeight
    }));
  };
  /**
   * 菜单展开底部内容
   *
   * @param {string} name
   * @return {*}
   */


  var menuFooterPersonalRender = function menuFooterPersonalRender(personalInfoDom, notificationDom) {
    return /*#__PURE__*/React.createElement("div", {
      className: "menuFooterPersonal"
    }, /*#__PURE__*/React.createElement("div", {
      className: "menuFooterName"
    }, personalInfoDom), /*#__PURE__*/React.createElement("div", {
      className: "menuFooterInformation"
    }, notificationDom));
  };

  var menuFooterCollapsePersonalRender = function menuFooterCollapsePersonalRender(personalInfoCollapseDom, notificationDom) {
    return /*#__PURE__*/React.createElement("div", {
      className: "menuFooterCollapsePersonal"
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        width: '56px',
        height: '56px',
        lineHeight: '56px',
        textAlign: 'center'
      }
    }, notificationDom), /*#__PURE__*/React.createElement("div", {
      style: {
        width: '56px',
        height: '56px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }
    }, personalInfoCollapseDom));
  };
  /**
   * 收起菜单栏
   * @param {any[]} dataSource
   * @return {*}
   */


  var siderbarCollapseMenu = function siderbarCollapseMenu(dataSource, defaultIcon) {
    return isValidArray(dataSource) && /*#__PURE__*/React.createElement("div", {
      className: "collapsedMenuContent"
    }, dataSource.map(function (node) {
      return /*#__PURE__*/React.createElement(React.Fragment, null, "ClosedSider") // <ClosedSider
      //   key={`${node.id}_${node.path}`}
      //   routes={node.children}
      //   name={node.funName}
      //   allRoutes={dataSource}
      // >
      //   <div
      //     className={
      //       node.id === selectedParentKeys[0]
      //         ? 'collapsedMenuIconWrapper collapsedMenuIconWrapperSelected'
      //         : 'collapsedMenuIconWrapper'
      //     }
      //   >
      //     <QuarkIcon type={node.icon || defaultIcon} style={{ fontSize: '20px' }} />
      //   </div>
      // </ClosedSider>
      ;
    }));
  }; // eslint-disable-next-line react-hooks/exhaustive-deps


  var collapseMenu = useMemo(function () {
    return siderbarCollapseMenu(currentMenuDataSource, defaultMenuIcon);
  }, [currentMenuDataSource, defaultMenuIcon, selectedParentKeys]); // eslint-disable-next-line react-hooks/exhaustive-deps

  var defaultMenu = useMemo(function () {
    return siderbarMenu();
  }, [currentMenuDataSource, expandedKeys, treeHeight, path, selectedParentKeys]); // 监听Menu区域高度动态设置tree高度

  var contonerHeight = (_useSize = useSize(siderbarMenuRef)) === null || _useSize === void 0 ? void 0 : _useSize.height;
  var contonerDebounHeight = useDebounce(contonerHeight, {
    wait: 200
  });
  useLayoutEffect(function () {
    // DOM 更新完成后打印出 div 的高度
    // console.log("contonerDebounHeight",contonerDebounHeight);
    if (!menuCollapse) {
      setTreeHeight(contonerDebounHeight);
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [menuCollapse, contonerDebounHeight]); // 搜索功能

  useEffect(function () {
    if (debouncedKeyWord.length > 0) {
      var _filterTreeData = filterTreeData(menuDataSource, debouncedKeyWord),
          filter = _filterTreeData.filter,
          filterKeys = _filterTreeData.filterKeys;

      setCurrentMenuDataSource(filter);
      var keys = [];
      filterKeys.forEach(function (key) {
        var path = treeFindPath(filter, function (node) {
          return node.id === key;
        });

        if (path.length > 0) {
          keys = keys.concat(path);
        }
      });
      setExpandedKeys(_toConsumableArray(new Set(keys))); // setAutoExpandAll(true);
    }

    if (debouncedKeyWord.length === 0) {
      // 数据初始化
      setCurrentMenuDataSource(menuDataSource);
      setExpandedKeys(menuExpandedKeys); // setAutoExpandAll(false);
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [debouncedKeyWord]); // 收缩展开

  useEffect(function () {
    setMenuCollapse(collapse);
  }, [collapse]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "menuWrapper",
    style: {
      width: menuCollapse ? '56px' : '240px'
    }
  }, menuHeader(menuCollapse), menuCollapse ? null : menuSearchRender(), dropdownMenu ? /*#__PURE__*/React.createElement(_Dropdown, {
    overlay: dropdownMenu,
    trigger: ['contextMenu']
  }, menuCollapse ? collapseMenu : defaultMenu) : /*#__PURE__*/React.createElement(React.Fragment, null, menuCollapse ? collapseMenu : defaultMenu), /*#__PURE__*/React.createElement("div", {
    className: "menuFooter",
    style: {
      height: menuCollapse ? '120px' : '56px'
    }
  }, menuCollapse && menuFooterCollapsePersonalRender(personalInfoCollapseRender, notificationRender), !menuCollapse && menuFooterPersonalRender(personalInfoRender, notificationRender))));
};

export default SideMenu;