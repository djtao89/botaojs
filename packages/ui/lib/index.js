"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Msgbox", {
  enumerable: true,
  get: function get() {
    return _MsgBox.default;
  }
});
Object.defineProperty(exports, "Split", {
  enumerable: true,
  get: function get() {
    return _Split.default;
  }
});

var _MsgBox = _interopRequireDefault(require("./components/MsgBox/"));

var _Split = _interopRequireDefault(require("./components/Split/"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }