"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.RESIZER_DEFAULT_CLASSNAME = void 0;

var _react = _interopRequireDefault(require("react"));

require("./index.less");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var RESIZER_DEFAULT_CLASSNAME = 'QuarkResizer';
exports.RESIZER_DEFAULT_CLASSNAME = RESIZER_DEFAULT_CLASSNAME;

var Resizer = function Resizer(props) {
  var className = props.className,
      _onDoubleClick = props.onDoubleClick,
      _onMouseDown = props.onMouseDown,
      _onTouchEnd = props.onTouchEnd,
      _onTouchStart = props.onTouchStart,
      resizerClassName = props.resizerClassName,
      split = props.split,
      style = props.style,
      toggleSize = props.toggleSize,
      isClick = props.isClick,
      resizerRef = props.resizerRef;
  var classes = [resizerClassName, split, className];
  var extraStyle = split === 'vertical' ? {
    display: 'inline-block',
    width: '8px',
    backgroundColor: '#f1f1f1',
    borderLeft: '1px solid #e2e2e2',
    borderRight: '1px solid #e2e2e2',
    cursor: 'col-resize'
  } : {
    display: 'inline-block',
    height: '8px',
    backgroundColor: '#f1f1f1',
    borderTop: '1px solid #e2e2e2',
    borderBottom: '1px solid #e2e2e2',
    cursor: 'row-resize'
  };
  var splitBtnStyle = split === 'vertical' ? {
    margin: 0,
    height: '100%',
    verticalAlign: 'middle',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 9,
    fontSize: '10px',
    color: '#000'
  } : {
    margin: 0,
    // position: 'absolute',
    // top: 0,
    // bottom: 0,
    height: '10px',
    width: '100%',
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 9,
    backgroundColor: 'rgb(241,241,241)',
    fontSize: '10px',
    color: '#000'
  };
  return /*#__PURE__*/_react.default.createElement("span", {
    role: "presentation",
    className: classes.join(' '),
    style: _objectSpread(_objectSpread(_objectSpread({}, style), extraStyle), splitBtnStyle),
    ref: resizerRef,
    onMouseDown: function onMouseDown(event) {
      return _onMouseDown(event);
    },
    onTouchStart: function onTouchStart(event) {
      event.preventDefault();

      _onTouchStart(event);
    },
    onTouchEnd: function onTouchEnd(event) {
      event.preventDefault();

      _onTouchEnd(event);
    },
    onDoubleClick: function onDoubleClick(event) {
      if (_onDoubleClick) {
        event.preventDefault();

        _onDoubleClick(event);
      } // if (isClick) {
      //   event.preventDefault();
      //   toggleSize();
      // }

    },
    onClick: function onClick(e) {
      if (isClick) {
        e.preventDefault();
        toggleSize();
      }
    }
  }, split === 'vertical' ? /*#__PURE__*/_react.default.createElement("div", {
    onClick: function onClick(event) {
      if (isClick) {
        event.preventDefault();
        toggleSize();
      }
    },
    className: "barLine"
  }) : /*#__PURE__*/_react.default.createElement("div", {
    onClick: function onClick(event) {
      if (isClick) {
        event.preventDefault();
        toggleSize();
      }
    },
    className: "$barLine2"
  }));
};

var _default = Resizer;
exports.default = _default;