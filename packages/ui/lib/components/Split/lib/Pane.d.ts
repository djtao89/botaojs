import React from 'react';
import type { CSSProperties } from 'styled-components';
declare type Prop = {
    children: any;
    className: string;
    split: 'vertical' | 'horizontal';
    style: CSSProperties;
    eleRef: any;
    size: number;
};
declare class Pane extends React.PureComponent<Prop> {
    render(): JSX.Element;
}
export default Pane;
