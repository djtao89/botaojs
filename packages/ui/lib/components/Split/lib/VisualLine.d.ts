import './index.less';
declare const _default: (props: {
    visualPosition: any;
    isShow: any;
    allow: any;
    split: any;
    style: any;
    primary: any;
    iToggleSize: any;
}) => JSX.Element;
export default _default;
