"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("antd/es/message/style");

var _message2 = _interopRequireDefault(require("antd/es/message"));

require("antd/es/notification/style");

var _notification2 = _interopRequireDefault(require("antd/es/notification"));

var _react = _interopRequireDefault(require("react"));

var _icons = require("@ant-design/icons");

var _ConfirmModal = _interopRequireDefault(require("./components/ConfirmModal"));

require("./index.less");

var _reactDom = _interopRequireDefault(require("react-dom"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var refreshContentId = 'botao_closeAll';
var myGlobal = globalThis || window;
var botaoNotificationControl = {
  app: _notification2.default,
  keyMap: new Map(),
  renderCloseAll: function renderCloseAll() {
    if (document.getElementById(refreshContentId)) {
      return;
    }

    var refreshContent = document.createElement('div');
    refreshContent.id = refreshContentId;
    document.body.append(refreshContent);

    var refresh = /*#__PURE__*/_react.default.createElement("div", {
      className: "botao_closeAll-content",
      onClick: function onClick() {
        var _myGlobal$closeAll;

        (_myGlobal$closeAll = myGlobal.closeAll) === null || _myGlobal$closeAll === void 0 ? void 0 : _myGlobal$closeAll.call(myGlobal);
        botaoNotificationControl.destoryCloseAll();
      }
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: "botao_closeAll-text"
    }, "\u4E00\u952E\u6E05\u7A7A"));

    _reactDom.default.render(refresh, document.getElementById(refreshContentId));
  },
  destoryCloseAll: function destoryCloseAll() {
    var noKey = true;

    _toConsumableArray(myGlobal.notificationList).forEach(function (app) {
      app.keyMap.forEach(function (_ref, key) {
        var onClose = _ref.onClose;
        noKey = false;
      });
    });

    if (noKey && document.getElementById(refreshContentId)) {
      var _document$getElementB;

      (_document$getElementB = document.getElementById(refreshContentId)) === null || _document$getElementB === void 0 ? void 0 : _document$getElementB.remove();
    }
  }
};
myGlobal.notificationList = myGlobal.notificationList || new Set();

if (!myGlobal.notificationList.has(botaoNotificationControl)) {
  myGlobal.notificationList.add(botaoNotificationControl);
}

myGlobal.closeAll = function () {
  _toConsumableArray(myGlobal.notificationList).forEach(function (app) {
    app.keyMap.forEach(function (_ref2, key) {
      var onClose = _ref2.onClose;
      app.app.close(key);
      onClose === null || onClose === void 0 ? void 0 : onClose();
      app.keyMap = new Map();
    });
  });
};

var custPrefix = 'botao-Msgbox';
/**
 * 适配消息框配置参数
 * @param {*} config 配置项
 */

var messageAdapter = function messageAdapter(config) {
  var option = _objectSpread({
    style: {
      marginTop: '38px'
    },
    druation: 3
  }, config);

  if (option.message) {
    option.content = option.message;
    delete option.message;
  }

  return option;
};

var notificationAdapter = function notificationAdapter(config) {
  var key = "".concat(new Date().getTime(), "_").concat(Math.random());
  botaoNotificationControl.keyMap.set(key, {
    onClose: config.onClose
  });
  setTimeout(function () {
    botaoNotificationControl.renderCloseAll();
  }, 50);

  var option = _objectSpread(_objectSpread({
    duration: 2,
    key: key,
    style: {
      width: 390,
      maxHeight: '500px',
      overflowY: 'auto',
      wordWrap: 'break-word'
    }
  }, config), {}, {
    onClose: function onClose() {
      var _config$onClose;

      (_config$onClose = config.onClose) === null || _config$onClose === void 0 ? void 0 : _config$onClose.call(config);
      botaoNotificationControl.keyMap.delete(key);
      botaoNotificationControl.destoryCloseAll();
    }
  });

  if (option.title) {
    option.message = option.title;
    delete option.title;
  }

  if (option.content) {
    option.description = option.content;
    delete option.content;
  }

  return option;
};

var MsgBox = {
  confirmModal: function confirmModal(config) {
    var myCancelButtonProps = {
      type: 'text',
      style: {
        marginRight: '0px'
      }
    };
    var cancelButtonProps = (config === null || config === void 0 ? void 0 : config.cancelButtonProps) ? _objectSpread(_objectSpread({}, config.cancelButtonProps), myCancelButtonProps) : myCancelButtonProps;
    return (0, _ConfirmModal.default)(_objectSpread(_objectSpread({}, config), {}, {
      modalType: 'Confirm',
      bodyStyle: {
        padding: '0 0px 16px 0px'
      },
      cancelButtonProps: cancelButtonProps
    }));
  },
  // info 提示
  infoModal: function infoModal(config) {
    return (0, _ConfirmModal.default)(_objectSpread({
      modalType: 'Info'
    }, config));
  },
  // success 提示
  successModal: function successModal(config) {
    return (0, _ConfirmModal.default)(_objectSpread({
      modalType: 'Success'
    }, config));
  },
  // error 提示
  errorModal: function errorModal(config) {
    return (0, _ConfirmModal.default)(_objectSpread({
      modalType: 'Error'
    }, config));
  },
  // warning 提示
  warningModal: function warningModal(config) {
    return (0, _ConfirmModal.default)(_objectSpread({
      modalType: 'Warning'
    }, config));
  },
  error: function error(config) {
    return _notification2.default.error(notificationAdapter(_objectSpread({
      duration: null,
      centered: true,
      className: "errorBackGround ".concat(!(config === null || config === void 0 ? void 0 : config.title) && !(config === null || config === void 0 ? void 0 : config.message) ? 'noNotificationTitle' : '', " ").concat(!(config === null || config === void 0 ? void 0 : config.content) && !(config === null || config === void 0 ? void 0 : config.description) ? 'noNotificationContent' : ''),
      icon: /*#__PURE__*/_react.default.createElement(_icons.CloseCircleFilled, {
        className: "".concat(custPrefix, "-msgBoxerr errorBackicon")
      }),
      placement: 'bottomRight'
    }, config)));
  },
  sucessNotification: function sucessNotification(config) {
    return _notification2.default.success(notificationAdapter(_objectSpread({
      duration: 3,
      centered: true,
      className: "sucessBackGround ".concat(!(config === null || config === void 0 ? void 0 : config.title) && !(config === null || config === void 0 ? void 0 : config.message) ? 'noNotificationTitle' : '', " ").concat(!(config === null || config === void 0 ? void 0 : config.content) && !(config === null || config === void 0 ? void 0 : config.description) ? 'noNotificationContent' : ''),
      icon: /*#__PURE__*/_react.default.createElement(_icons.CheckCircleTwoTone, {
        twoToneColor: "#52c41a"
      }),
      placement: 'topRight'
    }, config)));
  },
  infoNotification: function infoNotification(config) {
    return _notification2.default.info(notificationAdapter(_objectSpread({
      duration: 3,
      centered: true,
      className: "infoBackGround ".concat(!(config === null || config === void 0 ? void 0 : config.title) && !(config === null || config === void 0 ? void 0 : config.message) ? 'noNotificationTitle' : '', " ").concat(!(config === null || config === void 0 ? void 0 : config.content) && !(config === null || config === void 0 ? void 0 : config.description) ? 'noNotificationContent' : ''),
      icon: /*#__PURE__*/_react.default.createElement(_icons.InfoCircleOutlined, {
        className: "infoBackicon"
      }),
      placement: 'bottomRight'
    }, config)));
  },
  warningNotification: function warningNotification(config) {
    return _notification2.default.warning(notificationAdapter(_objectSpread({
      duration: 3,
      centered: true,
      className: "warningBackGround ".concat(!(config === null || config === void 0 ? void 0 : config.title) && !(config === null || config === void 0 ? void 0 : config.message) ? 'noNotificationTitle' : '', " ").concat(!(config === null || config === void 0 ? void 0 : config.content) && !(config === null || config === void 0 ? void 0 : config.description) ? 'noNotificationContent' : ''),
      icon: /*#__PURE__*/_react.default.createElement(_icons.ExclamationCircleOutlined, {
        className: "warningBackicon"
      }),
      placement: 'bottomRight'
    }, config)));
  },
  open: function open(config) {
    return _notification2.default.open(notificationAdapter(_objectSpread({
      placement: 'bottomRight'
    }, config)));
  },
  info: function info(config) {
    return _message2.default.info(_objectSpread(_objectSpread({}, messageAdapter(config)), {}, {
      className: "".concat(custPrefix, "-myMessage ").concat(custPrefix, "-info")
    }));
  },
  success: function success(config) {
    return _message2.default.success(_objectSpread(_objectSpread({}, messageAdapter(config)), {}, {
      className: "".concat(custPrefix, "-myMessage ").concat(custPrefix, "-success")
    }));
  },
  warning: function warning(config) {
    return _message2.default.warning(_objectSpread(_objectSpread({}, messageAdapter(config)), {}, {
      className: "".concat(custPrefix, "-myMessage ").concat(custPrefix, "-warning")
    }));
  },
  config: function config(_config) {
    return _notification2.default.config(notificationAdapter(_objectSpread({
      placement: 'bottomRight'
    }, _config)));
  },
  close: function close(key) {
    return _notification2.default.close(key);
  },
  destroy: function destroy() {
    return _notification2.default.destroy();
  },
  getMsgBox: function getMsgBox() {
    return _notification2.default;
  },
  getConfig: function getConfig(config) {
    return notificationAdapter(config);
  }
};
var _default = MsgBox;
exports.default = _default;