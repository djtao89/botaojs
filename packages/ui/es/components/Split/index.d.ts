import Pane from './lib/Pane';
import type { SplitProps } from './interface';
export declare const SplitPane: (props: SplitProps) => JSX.Element;
declare const Split: {
    Pane: typeof Pane;
    SplitPane: (props: SplitProps) => JSX.Element;
};
export default Split;
