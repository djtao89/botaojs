import React from 'react';
declare class SplitPane extends React.Component<any, any> {
    splitPane: any;
    pane1: any;
    pane2: any;
    constructor(props: any);
    resizerRef: React.RefObject<HTMLDivElement>;
    deviation: number;
    componentDidMount(): void;
    static getDerivedStateFromProps(nextProps: any, prevState: any): any;
    componentWillUnmount(): void;
    onMouseDown(event: any): void;
    onTouchStart(event: any): void;
    onMouseMove(event: any): void;
    onTouchMove(event: any): void;
    onMouseUp(event: any): void;
    static getSizeUpdate(props: any, state: any): any;
    render(): JSX.Element;
}
export default SplitPane;
