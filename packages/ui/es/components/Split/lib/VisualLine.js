function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable no-nested-ternary */
import React from 'react';
import './index.less';
export default (function (props) {
  var visualPosition = props.visualPosition,
      isShow = props.isShow,
      allow = props.allow,
      split = props.split,
      style = props.style;
  var width = split === 'vertical' ? '8px' : '100%';
  var left = split === 'vertical' ? visualPosition : 0;
  var right = split === 'vertical' ? undefined : 0;
  var top = split === 'vertical' ? 0 : visualPosition;
  var bottom = split === 'vertical' ? 0 : undefined;
  var cursor = split === 'vertical' ? 'col-resize' : 'row-resize';
  var extraStyle = split === 'vertical' ? {
    display: 'inline-block',
    width: '8px',
    backgroundColor: '#f1f1f1',
    borderLeft: '1px solid #e1eeff',
    borderRight: '1px solid #d3e4ff',
    cursor: 'col-resize'
  } : {
    display: 'inline-block',
    height: '8px',
    backgroundColor: '#e1eeff',
    borderTop: '1px solid #d3e4ff',
    borderBottom: '1px solid #d3e4ff',
    cursor: 'row-resize'
  };
  var splitBtnStyle = split === 'vertical' ? {
    width: '100%',
    margin: 0,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 9,
    backgroundColor: '#e1eeff',
    fontSize: '7px',
    color: '#000',
    cursor: 'row-resize'
  } : {
    margin: 0,
    position: 'absolute',
    top: 0,
    bottom: 0,
    height: '7px',
    width: '100%',
    left: 0,
    right: 0,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 9,
    backgroundColor: '#e1eeff',
    color: '#000',
    fontSize: '10px'
  };
  return /*#__PURE__*/React.createElement("span", {
    role: "presentation",
    style: _objectSpread(_objectSpread(_objectSpread({}, style), extraStyle), {}, {
      position: 'absolute',
      top: top,
      bottom: bottom,
      left: left,
      right: right,
      width: width,
      // border: border,
      pointerEvents: 'none',
      zIndex: 99999999,
      display: isShow ? 'block' : 'none',
      cursor: cursor // opacity

    })
  }, /*#__PURE__*/React.createElement("a", {
    style: splitBtnStyle
  }, split === 'vertical' ? /*#__PURE__*/React.createElement("div", {
    className: "barLine"
  }) : /*#__PURE__*/React.createElement("div", {
    className: "barLine2"
  })));
});