import React, { useState } from 'react';
import { Button } from 'antd';
import { Msgbox } from '@d-botao/ui';

const Index = () => {
  const confirm = () => {
    Msgbox.confirmModal({
      modalType: 'Confirm',
      title: '审核',
      content: '是否确定审核未审核的记录?',
      onOk: () => {},
    });
  };
  const confirm2 = () => {
    Msgbox.confirmModal({
      modalType: 'Confirm',
      title: '审核',
      content: '是否确定审核未审核的记录?',
      okText: '是',
      cancelText: '取消',
      onOk: () => {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            console.log('Oops errors!');
            resolve('成功了');
          }, 1000);
        });
      },
    });
  };

  const confirm3 = () => {
    Msgbox.confirmModal({
      title: '自定义按钮文字、类型',
      content: 'some messages...some messages...',
      okText: 'okText',
      cancelText: 'cancelText',
      cancelButtonProps: { type: 'dashed', danger: true },
      okType: 'dashed',
      extraFooter: [
        <Button
          {...{ order: 50 }}
          onClick={() => {
            console.log('点击了拓展按钮');
          }}
          type="primary"
        >
          拓展按钮
        </Button>,
      ],
      onOk: () => {
        alert('点击okText');
      },
    });
  };

  const infoModal = () => {
    Msgbox.infoModal({
      title: 'This is a infoModal message',
      content: 'some messages...some messages...',
    });
  };

  const successModal = () => {
    Msgbox.successModal({
      title: 'This is a successModal message',
      content: 'some messages...some messages...',
    });
  };
  const errorModal = () => {
    Msgbox.errorModal({
      title: 'This is a errorModal message',
      content: 'some messages...some messages...',
    });
  };
  const warningModal = () => {
    Msgbox.warningModal({
      title: 'This is a warningModal message',
      content: 'some messages...some messages...',
    });
  };
  return (
    <div>
      <Button
        onClick={() => {
          infoModal();
        }}
      >
        消息提示Info
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          successModal();
        }}
      >
        消息提示Success
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          errorModal();
        }}
      >
        消息提示Error
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          warningModal();
        }}
      >
        消息提示Warning
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          confirm();
        }}
      >
        确认提示confirm
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          confirm2();
        }}
      >
        确认提示拓展
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          confirm3();
        }}
      >
        自定义按钮文字、类型
      </Button>{' '}
      &nbsp;
    </div>
  );
};

export default Index;
