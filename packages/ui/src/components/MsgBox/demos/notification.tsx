import React, { useState } from 'react';
import { Button } from 'antd';
import { Msgbox } from '@d-botao/ui';

const Index = () => {
  const confirm = () => {
    Msgbox.error({
      // // title: '',
      // title: '123',
      // // content: '',
      // content: '您的网络发生异常',
      message: '错误',
      description: '您的网络发生异常',
    });
  };

  const success = () => {
    Msgbox.sucessNotification({
      message: '成功',
      description: '成功成功成功成功',
    });
  };

  const info = () => {
    Msgbox.infoNotification({
      message: 'info',
      description: 'infoinfoinfoinfo',
    });
  };

  const warning = () => {
    Msgbox.warningNotification({
      message: 'warning',
      description: 'warningwarningwarning',
    });
  };
  return (
    <div>
      <Button
        onClick={() => {
          confirm();
        }}
      >
        error
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          success();
        }}
      >
        success
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          info();
        }}
      >
        info
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          warning();
        }}
      >
        warning
      </Button>
    </div>
  );
};

export default Index;
