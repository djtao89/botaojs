import React, { useState } from 'react';
import { Button } from 'antd';
import { Msgbox } from '@d-botao/ui';

const Index = () => {
  return (
    <div>
      <Button
        onClick={() => {
          Msgbox.success({
            message: '成功成功成功成功',
          });
        }}
      >
        success
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          Msgbox.info({
            message: 'infoinfoinfoinfoinfo',
          });
        }}
      >
        info
      </Button>{' '}
      &nbsp;
      <Button
        onClick={() => {
          Msgbox.warning({
            message:
              'warningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarning',
          });
        }}
      >
        warning
      </Button>
    </div>
  );
};

export default Index;
