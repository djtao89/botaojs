import React from 'react';
import { message, notification } from 'antd';
import {
  CloseCircleFilled,
  CheckCircleTwoTone,
  InfoCircleOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import toolModal from './components/ConfirmModal';
import './index.less';
// import botaoIcon from '../botaoIcon';
import ReactDOM from 'react-dom';

const refreshContentId = 'botao_closeAll';

const myGlobal: any = globalThis || window;

const botaoNotificationControl = {
  app: notification,
  keyMap: new Map(),
  renderCloseAll: () => {
    if (document.getElementById(refreshContentId)) {
      return;
    }
    const refreshContent = document.createElement('div');
    refreshContent.id = refreshContentId;
    document.body.append(refreshContent);
    const refresh = (
      <div
        className="botao_closeAll-content"
        onClick={() => {
          myGlobal.closeAll?.();
          botaoNotificationControl.destoryCloseAll();
        }}
      >
        <span className="botao_closeAll-text">一键清空</span>
        {/* <botaoIcon type="a-bianzu2" /> */}
      </div>
    );
    ReactDOM.render(refresh, document.getElementById(refreshContentId));
  },
  destoryCloseAll: () => {
    let noKey = true;
    [...myGlobal.notificationList].forEach((app) => {
      app.keyMap.forEach(({ onClose }: any, key: string) => {
        noKey = false;
      });
    });
    if (noKey && document.getElementById(refreshContentId)) {
      document.getElementById(refreshContentId)?.remove();
    }
  },
};

myGlobal.notificationList = myGlobal.notificationList || new Set();
if (!myGlobal.notificationList.has(botaoNotificationControl)) {
  myGlobal.notificationList.add(botaoNotificationControl);
}

myGlobal.closeAll = () => {
  [...myGlobal.notificationList].forEach((app) => {
    app.keyMap.forEach(({ onClose }: any, key: string) => {
      app.app.close(key);
      onClose?.();
      app.keyMap = new Map();
    });
  });
};

const custPrefix = 'botao-Msgbox';
/**
 * 适配消息框配置参数
 * @param {*} config 配置项
 */
const messageAdapter = (config: any) => {
  const option = {
    style: {
      marginTop: '38px',
    },
    druation: 3,
    ...config,
  };
  if (option.message) {
    option.content = option.message;
    delete option.message;
  }

  return option;
};

const notificationAdapter = (config: any) => {
  const key = `${new Date().getTime()}_${Math.random()}`;
  botaoNotificationControl.keyMap.set(key, {
    onClose: config.onClose,
  });
  setTimeout(() => {
    botaoNotificationControl.renderCloseAll();
  }, 50);
  const option = {
    duration: 2,
    key,
    style: { width: 390, maxHeight: '500px', overflowY: 'auto', wordWrap: 'break-word' },
    ...config,
    onClose: () => {
      config.onClose?.();
      botaoNotificationControl.keyMap.delete(key);
      botaoNotificationControl.destoryCloseAll();
    },
  };
  if (option.title) {
    option.message = option.title;
    delete option.title;
  }
  if (option.content) {
    option.description = option.content;
    delete option.content;
  }
  return option;
};

const MsgBox = {
  confirmModal(config: any) {
    const myCancelButtonProps = {
      type: 'text',
      style: { marginRight: '0px' },
    };
    const cancelButtonProps: any = config?.cancelButtonProps
      ? { ...config.cancelButtonProps, ...myCancelButtonProps }
      : myCancelButtonProps;
    return toolModal({
      ...config,
      modalType: 'Confirm',
      bodyStyle: { padding: '0 0px 16px 0px' },
      cancelButtonProps,
    });
  },

  // info 提示
  infoModal(config: any) {
    return toolModal({ modalType: 'Info', ...config });
  },
  // success 提示
  successModal(config: any) {
    return toolModal({ modalType: 'Success', ...config });
  },
  // error 提示
  errorModal(config: any) {
    return toolModal({ modalType: 'Error', ...config });
  },
  // warning 提示
  warningModal(config: any) {
    return toolModal({ modalType: 'Warning', ...config });
  },

  error(config: any) {
    return notification.error(
      notificationAdapter({
        duration: null,
        centered: true,
        className: `errorBackGround ${
          !config?.title && !config?.message ? 'noNotificationTitle' : ''
        } ${!config?.content && !config?.description ? 'noNotificationContent' : ''}`,
        icon: <CloseCircleFilled className={`${custPrefix}-msgBoxerr errorBackicon`} />,
        placement: 'bottomRight',
        ...config,
      }),
    );
  },

  sucessNotification(config: any) {
    return notification.success(
      notificationAdapter({
        duration: 3,
        centered: true,
        className: `sucessBackGround ${
          !config?.title && !config?.message ? 'noNotificationTitle' : ''
        } ${!config?.content && !config?.description ? 'noNotificationContent' : ''}`,
        icon: <CheckCircleTwoTone twoToneColor="#52c41a" />,
        placement: 'topRight',
        ...config,
      }),
    );
  },

  infoNotification(config: any) {
    return notification.info(
      notificationAdapter({
        duration: 3,
        centered: true,
        className: `infoBackGround ${
          !config?.title && !config?.message ? 'noNotificationTitle' : ''
        } ${!config?.content && !config?.description ? 'noNotificationContent' : ''}`,
        icon: <InfoCircleOutlined className="infoBackicon" />,
        placement: 'bottomRight',
        ...config,
      }),
    );
  },

  warningNotification(config: any) {
    return notification.warning(
      notificationAdapter({
        duration: 3,
        centered: true,
        className: `warningBackGround ${
          !config?.title && !config?.message ? 'noNotificationTitle' : ''
        } ${!config?.content && !config?.description ? 'noNotificationContent' : ''}`,
        icon: <ExclamationCircleOutlined className="warningBackicon" />,
        placement: 'bottomRight',
        ...config,
      }),
    );
  },

  open(config: any) {
    return notification.open(notificationAdapter({ placement: 'bottomRight', ...config }));
  },
  info(config: any) {
    return message.info({
      ...messageAdapter(config),
      className: `${custPrefix}-myMessage ${custPrefix}-info`,
    });
  },
  success(config: any) {
    return message.success({
      ...messageAdapter(config),
      className: `${custPrefix}-myMessage ${custPrefix}-success`,
    });
  },
  warning(config: any) {
    return message.warning({
      ...messageAdapter(config),
      className: `${custPrefix}-myMessage ${custPrefix}-warning`,
    });
  },
  config(config: any) {
    return notification.config(notificationAdapter({ placement: 'bottomRight', ...config }));
  },
  close(key: any) {
    return notification.close(key);
  },
  destroy() {
    return notification.destroy();
  },

  getMsgBox() {
    return notification;
  },
  getConfig(config: any) {
    return notificationAdapter(config);
  },
};

export default MsgBox;
