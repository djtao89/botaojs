// modal型弹窗，可拖拽，MSGBOX专用
import { globalConfig } from 'antd/es/config-provider';
import React, { useState } from 'react';
import ReactDOM from 'react-dom';
// import QuarkModal from '../../../QuarkModal';
import { Modal } from 'antd';

const ConfirmModal = (props: any) => {
  const { onCancel, onOk, getContainer, extraFooter = [], ...rest } = props;
  const prefixCls = globalConfig().getPrefixCls();
  // console.log(111, prefixCls);
  const [visible, setVisible] = useState(true);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const handleCancel = async () => {
    let iCheck = true;
    if (onCancel) {
      iCheck = await onCancel();
    }

    if (iCheck) {
      setVisible(false);
      setTimeout(() => {
        const modalNode: any = document.querySelector('#toolModal');
        document.body.removeChild(modalNode);
      }, 300);
    }
  };

  const handleOk = async () => {
    setConfirmLoading(true);
    let iCheck = true;
    if (onOk) {
      iCheck = await onOk();
    }
    setVisible(false);
    setTimeout(() => {
      const modalNode: any = document.querySelector('#toolModal');
      document.body.removeChild(modalNode);
    }, 300);
  };

  const myExtraFooter = extraFooter.map((component: any) => {
    const handleClick = async () => {
      if (component.props.onClick) {
        const isClose = await component.props.onClick();
      }
      setVisible(false);
      setTimeout(() => {
        const modalNode: any = document.querySelector('#toolModal');
        document.body.removeChild(modalNode);
      }, 300);
    };
    return React.cloneElement(component, {
      onClick: handleClick,
    });
  });
  return (
    <>
      <Modal
        onCancel={handleCancel}
        prefixCls={prefixCls}
        onOk={handleOk}
        visible={visible}
        NodeInfo={null}
        extraFooter={myExtraFooter}
        confirmLoading={confirmLoading}
        bodyStyle={{
          height: '100px',
          overflowY: 'auto',
        }}
        centered={true}
        {...rest}
        destroyOnClose={true}
        // getContainer={document.querySelector('#toolModal')}
      />
    </>
  );
};

const toolModal = (props: any) => {
  let div: any = null;
  if (document.querySelector('#toolModal')) {
    div = document.querySelector('#toolModal');
  } else {
    div = document.createElement('div');
    div.id = 'toolModal';
  }
  document.body.appendChild(div);
  ReactDOM.render(<ConfirmModal {...props} />, div);
};

export default toolModal;
