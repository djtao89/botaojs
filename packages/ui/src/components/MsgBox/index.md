---
order: 6
title: Msgbox - 提示弹窗
group:
  title: 基础功能组件
  path: /basicFun
nav:
  title: ui
  path: /ui
---

### botaoModal 模式

信息提示框引入`botaoMsgbox`(信息提示弹窗的封装),属性值：`confirmModal、infoModal、successModal、errorModal、warningModal`;

需要设置`modalType`属性值：`Info、Success、Error、Warning、Confirm`;设置对应的`title`,`content`; <code src="./demos/basic.tsx"  title="基础用法" />

### 拓展 notification，持续更新中。。。

信息提示框引入`botaoMsgbox`(信息提示弹窗的封装),属性值：`error、sucessNotification、infoNotification、warningNotification`; <code src="./demos/notification.tsx"  title="notification" />

### 拓展 message，持续更新中。。。

信息提示框引入`botaoMsgbox`(信息提示弹窗的封装),属性值`success、info、warning`; <code src="./demos/message.tsx"  title="message" />

<!-- <API> -->
