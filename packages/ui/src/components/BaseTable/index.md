---
order: 6
title: BaseTable - 基础表格
group:
  title: 基础功能组件
  path: /basicFun
nav:
  title: ui
  path: /ui
---

```tsx
import React from 'react';
import { Button } from 'antd';

export default () => <Button>123</Button>;
```

<code src="./demos/base.tsx"  title="基础用法" />

<!-- <API> -->
