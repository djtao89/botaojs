import React, { useLayoutEffect } from 'react';
import {
  BaseTable,
  BaseTableProps,
  getTreeDepth,
  LoadingContentWrapperProps,
} from 'ali-react-table';
import cx from 'classnames';
import { useEffect, useState } from 'react';
import styled, { keyframes } from 'styled-components';
import { icons } from './common-views';
import basePropsTransfer from './basePropsTransfer';

const loadingIconRotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const StyledBaseTable = styled(BaseTable)`
  --line-height: 1.5715;
  --font-size: 14px;
  --row-height: 22px;
  --header-row-height: 36px;
  --cell-padding: 8px;

  --lock-shadow: rgba(0, 0, 0, 0.2) 0 0 10px 0px;
  --border-color: #f0f0f0;
  --color: rgba(0, 0, 0, 0.85);
  --bgcolor: white;
  --hover-bgcolor: #fafafa;
  --highlight-bgcolor: #fafafa;
  --header-color: rgba(0, 0, 0, 0.85);
  --header-bgcolor: #fafafa;
  --header-hover-bgcolor: #f5f5f5;
  --header-highlight-bgcolor: #f5f5f5;

  &.dark {
    --lock-shadow: black 0 0px 6px 2px;
    --border-color: #f0f0f0;
    --color: rgba(255, 255, 255, 0.65);
    --bgcolor: #141414;
    --hover-bgcolor: #262626;
    --highlight-bgcolor: #262626;
    --header-color: rgba(255, 255, 255, 0.85);
    --header-bgcolor: #1d1d1d;
    --hover-hover-bgcolor: #222;
    --header-highlight-bgcolor: #222;
  }

  &.aliSizelarge {
    --cell-padding: 7px 8px 8px 8px;
    --line-height: 22px;
    --font-size: 14px;
    --row-height: 22px;
    --header-row-height: 38px;
  }
  &.aliSizemiddle {
    --cell-padding: 3px 8px 4px 8px;
    --line-height: 22px;
    --font-size: 12px;
    --row-height: 22px;
    --header-row-height: 30px;
  }
  &.aliSizesmall {
    --cell-padding: 1px 4px 1px 4px;
    --line-height: 19px;
    --font-size: 12px;
    --row-height: 19px;
    --header-row-height: 26px;
  }

  td {
    transition: background 0.3s;
    // background: inherit;
  }

  th {
    font-weight: 500;
  }

  .art-table-header-cell > span {
    margin-left: 0px !important;
  }
  .art-table-cell > .expansion-cell {
    width: 100%;
  }
  .art-table-cell > .expansion-cell > span {
    width: calc(100% - 24px);
  }
  .ant-table-cell-ellipsis > .expansion-cell {
    width: 100%;
  }
  .ant-table-cell-ellipsis > .expansion-cell.expanded,
  .ant-table-cell-ellipsis > .expansion-cell.collapsed {
    display: inline;
  }
  .ant-table-cell-ellipsis > .expansion-cell > .expansion-icon {
    position: relative;
    top: 2px;
  }
  .ant-table-cell-ellipsis > .expansion-cell > span {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    word-break: keep-all;
  }

  &:not(.bordered) {
    --cell-border-vertical: none;
    --header-cell-border-vertical: none;

    thead > tr.first th {
      border-top: none;
    }
  }
  &.use-outer-border:not(.has-footer) tbody tr.last td {
    border-bottom: var(--cell-border-horizontal);
  }
  &.use-outer-border:not(.has-footer) tbody tr.last.no-hover td {
    border-bottom: none;
  }
  &,
  .art-horizontal-scroll-container {
    ::-webkit-scrollbar {
      background: #fff;
    }
  }

  .single-select td:first-of-type {
    border-left: 4px solid var(--primary-color) !important;
  }
  .drag-over {
    text-decoration: underline;
    background: #e1ebc5 !important;
  }

  .drag-over-after-bottom td {
    color: red;
    border-bottom: 2px solid var(--primary-color) !important;
  }

  .drag-over-top-top td {
    border-top: 2px solid var(--primary-color) !important;
  }

  .drag-over-disabled td {
    background: pink;
    cursor: not-allowed !important;
  }
  .drag-over td {
    background: inherit;
  }
  /*高亮样式*/
  .drag-over-bottom td {
    border-bottom: 2px dashed var(--primary-color) !important;
  }
  .drag-over-top td {
    border-top: 2px dashed var(--primary-color) !important;
  }

  // #components-table-demo-drag-sorting tr.drop-over-upward td {
  //   border-top: 2px dashed var(--primary-color);
  // }
  /*列拖拽样式*/
  .drag-left {
    border-left: 2px dashed var(--primary-color) !important;
  }
  .drag-right {
    border-right: 2px dashed var(--primary-color) !important;
  }
` as unknown as typeof BaseTable;

const HippoEmptyContent = React.memo(() => (
  <>
    <img
      alt="hippo-table-empty"
      width="160"
      height="160"
      src="https://img.alicdn.com/tfs/TB1s7zrWeL2gK0jSZFmXXc7iXXa-240-240.svg"
    />
    <div className="empty-tips" style={{ marginTop: -24 }}>
      没有符合查询条件的数据
      <br />
      请修改条件后重新查询
    </div>
  </>
));

type CellRenderProps = {
  row: any;
  rowIndex: any;
  column: any;
  colIndex: any;
  tdProps: any;
};

const CellRender = React.memo((props: CellRenderProps) => {
  // row 单元格的行数据
  // rowIndex 单元格的行下标
  // column 单元格的列配置
  // colIndex 单元格的列下标
  // tdProps <td> 元素的 props
  const { tdProps } = props;
  return <td {...tdProps} />;
});

// 加载过程 持续一秒钟之后 再展示 加载按钮
const LOADING_SHOW_DELAY = 1000;

interface LoadingContentWrapperState {
  warming: boolean;
  prevVisible: boolean;
}

class HippoLoadingContentWrapper extends React.Component<
  LoadingContentWrapperProps,
  LoadingContentWrapperState
> {
  handle: any;

  state = {
    warming: false,
    prevVisible: false,
  };

  static getDerivedStateFromProps(
    props: Readonly<LoadingContentWrapperProps>,
    state: Readonly<LoadingContentWrapperState>,
  ) {
    if (!state.prevVisible && props.visible) {
      return { warming: true, prevVisible: true };
    }
    if (!props.visible) {
      return { warming: false, prevVisible: false };
    }
    return null;
  }

  componentDidUpdate(prevProps: Readonly<LoadingContentWrapperProps>) {
    if (!prevProps.visible && this.props.visible) {
      this.handle = setTimeout(() => {
        this.setState({ warming: false });
      }, LOADING_SHOW_DELAY);
    } else if (!this.props.visible) {
      clearTimeout(this.handle);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.handle);
  }

  render() {
    const { visible, children } = this.props;
    const { warming } = this.state;

    return (
      <div
        className="hippo-loading-content-wrapper"
        style={{
          filter: visible ? (warming ? 'blur(0.5px)' : 'blur(1px)') : 'none',
          transition: 'filter 300ms cubic-bezier(0, 0.4, 0.6, 1)',
        }}
      >
        {children}
      </div>
    );
  }
}

const StyledSvgIcon = styled(icons.Loading)`
  display: block;
  margin: auto;
  width: 40px;
  height: 40px;
  animation: 1.5s linear 0s infinite ${loadingIconRotate};
  opacity: 1;
  transition: opacity 300ms;
`;

const HippoLoadingIcon = React.memo(() => {
  const [show, setShow] = useState(false);

  useEffect(() => {
    const handle = setTimeout(() => {
      setShow(true);
    }, LOADING_SHOW_DELAY);

    return () => clearTimeout(handle);
  }, []);

  return (
    <StyledSvgIcon
      type="loading"
      className="hippo-table-loading"
      style={{ opacity: show ? 1 : 0 }}
    />
  );
});

/** Hippo Design 基础表格组件
 *
 * HippoBaseTable 在 ali-react-table 提供的 BaseTable 基础上定制了默认的表格样式。  */
export default React.forwardRef<BaseTable, BaseTableProps>(function (props: any, ref) {
  // 转换
  const art_props = basePropsTransfer(props);

  const { tableWrapper, tableHeight = 500 } = art_props;

  useLayoutEffect(() => {
    if (tableHeight > 0 && tableWrapper?.current) {
      const domStyle = tableWrapper.current.querySelector(
        '.art-horizontal-scroll-container',
      )?.style;
      domStyle.minHeight = `${tableHeight - 41}px`;
    }
  });

  const headerDepth = getTreeDepth(props.columns);
  // console.log(props)

  return (
    <StyledBaseTable
      ref={ref}
      {...art_props}
      className={cx(props.className, {
        'header-depth-0': headerDepth === 0,
        'header-depth-1': headerDepth === 1,
      })}
      components={{
        EmptyContent: HippoEmptyContent,
        LoadingContentWrapper: HippoLoadingContentWrapper,
        LoadingIcon: HippoLoadingIcon,
        Cell: CellRender,
        ...props.components,
      }}
      style={{ height: tableHeight, overflow: 'auto' }}
    />
  );
});
