import { useTablePipeline, features } from 'ali-react-table';
// import { Checkbox } from '@alifd/next';
import { Checkbox } from 'antd';

// 转换antd风格api到aliReactTable
export default (props: any) => {
  const {
    rowKey = 'id',
    trKey,
    selectMode,
    bordered = true,
    borderedLine = false,
    className,
    // className = 'compact',
    size = 'large', // 表格模式
    useVirtual = { horizontal: 10, vertical: true, header: 'auto' },
    useOuterBorder = true,
    defaultColumnWidth,
    estimatedRowHeight = 39,
    loading,
    tableHeight,
    dataSource: defaultDataSource,
    columns: defaultColumns,
    getRowProps,
    rowEditActionType = false,
    treeMode,
    sortMode,
    currentRecord,
    // selectedRowKeys,
    expandedRowKeys,
    featuresOptions,
    dragColumnWidth = true,
    onChangeSelectedKeys,
    onDataChange,
    useEdit = false,
    components = {},
    valueTypes = {},
    useFrameSelection: propsUseFrameSelection = false,
    onFrameSelectionsChange,
    checkboxDisabled,
    isShowReversCheck,
    adaptiveCol,
    setAdaptiveCol,
    sortHeader,
    filterMode,
    rowSelection,
    columns,
    dataSource,
    // onChange,
    ...tableProps
  } = props;

  // const { rowKey = "id", onChange:any, columns, dataSource, ...rest } = props

  const { selectedRowKeys, onChange } = rowSelection;

  columns.forEach((column: any) => {
    column.code = column.key;
    column.name = column.dataIndex;
  });

  const pipeline = useTablePipeline({ components: { Checkbox } })
    .input({
      dataSource,
      columns: columns.map((column: any) => {
        const { sorter } = column;
        const options: any = {};
        if (sorter !== false) {
          options.sortable = sorter || true;
          // useSort = true;
        }
        return {
          code: column.key,
          lock: column.fixed,
          headerCellProps: { className: 'ant-table-cell-ellipsis' },
          getValue: (record: any) => record[column.key],
          getCellProps: (value: any, record: any, rowIndex: number) => {
            const { ellipsis, getCellProps } = column;
            const cellProps = (getCellProps && getCellProps(value, record, rowIndex)) || {};
            if (ellipsis) {
              cellProps.className = 'ant-table-cell-ellipsis';
            }
            return cellProps;
          },
          features: options,
          ...column,
        };
      }),
    })
    .primaryKey(rowKey) // 每一行数据由 id 字段唯一标记
    .use(
      features.multiSelect({
        highlightRowWhenSelected: false,
        defaultValue: [],
        defaultLastKey: '1',
        checkboxPlacement: 'start',
        checkboxColumn: { lock: true },
        clickArea: 'cell',
        onChange: onChange ? (...restChangeArgs) => onChange(...restChangeArgs) : undefined,
      }),
    );

  // pipeline.primaryKey(rowKey).input({
  //   dataSource: searchRows(item => {
  //     // 如果没有可显示的叶子节点即不显示了
  //     return (item.isLeaf || !item.children?.length) && !item.hidden;
  //   }, dataSource),
  //  ,
  // });

  const newProps = {
    ...pipeline.getProps(),
    isLoading: loading,
    emptyCellHeight: tableHeight - 50,
    ...tableProps,
  };

  return newProps;
};
