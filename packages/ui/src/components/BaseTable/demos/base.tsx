import React from 'react';
import { BaseTable } from '@d-botao/ui';

const columns = [
  {
    title: '接口地址',
    dataIndex: 'url',
    key: 'url',
    // render: text => <a>{text}</a>
    // render: (content, record, index) => {
    //   return content;
    // },
  },
  {
    title: '接口别名',
    dataIndex: 'alias',
    key: 'alias',
  },

  {
    title: '请求方法',
    dataIndex: 'methods',
    key: 'methods',
    // render: tags => (
    //   <>
    //     {tags.map(tag => {
    //       let color = tag.length > 5 ? "geekblue" : "green";
    //       if (tag === "loser") {
    //         color = "volcano";
    //       }
    //       return (
    //         <Tag color={color} key={tag}>
    //           {tag.toUpperCase()}
    //         </Tag>
    //       );
    //     })}
    //   </>
    // )
  },

  {
    title: '描述',
    dataIndex: 'description',
    key: 'description',
  },
  {
    title: '是否启用mock',
    dataIndex: 'isMock',
    key: 'isMock',
    // render: (content) => <>{content ? '是' : '否'}</>,
  },
  {
    title: '操作',
    key: 'action',
    dataIndex: 'isMock',

    // render: (...args) => {
    //   return (
    //     <Space size="middle">
    //       <a>详情</a>
    //       <a>测试</a>
    //       <a onClick={(record) => handles.delete([args[1]])}>删除</a>
    //     </Space>
    //   );
    // },
  },
];

const dataSource = [];

for (let i = 0; i < 100; i++) {
  dataSource.push({
    id: i,
    url: 'url1',
    alias: 'alias' + i,
    methods: 'post',
    isMock: Math.random() > 0.5,
    description: 'asda',
  });
}

const selectedRowKeys: string[] = [];

export default () => (
  <>
    <BaseTable
      rowKey="id"
      dataSource={dataSource}
      columns={columns}
      // handleDelete={handleDelete}
      rowSelection={{
        selectedRowKeys,
        onChange: (...args: any[]) => {
          console.log(args);
        },
      }}
    />
  </>
);
