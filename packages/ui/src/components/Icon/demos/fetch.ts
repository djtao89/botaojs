// fetch请求库
// const requestTimeout = 30000; // 请求超时。
// const loadingTimeout = 0; // 等待界面显示超时。
// const token = sessionStorage.platFormToken

type OptionType = {
  headers: Headers | string[][] | Record<string, string> | undefined;
  params:
    | Blob
    | BufferSource
    | FormData
    | URLSearchParams
    | ReadableStream<Uint8Array>
    | string
    | undefined;
  // method: "POST" | "GET" | "PUT" | "DELETE"
};

const defaultOptions: OptionType = {
  headers: undefined,
  params: undefined,
  // method: "POST",
};

export default class Fetch {
  intercepter: any;
  request: any;
  errorHandler: any;
  controller: any;
  constructor() {
    this.request = (url: string, options: any) => {
      options = options ? options : defaultOptions;
      const { headers, params, method } = options;
      return new Promise((resolve, reject) => {
        fetch(url, {
          method,
          headers,
          body: params,
        })
          .then((response: any) => {
            resolve(response);
          })
          .catch((err) => {
            resolve(err);
          });
      });
    };

    this.errorHandler = (err: any) => {
      console.log('错误捕获', err);
      const { status } = err;
      const codeMessage: any = {
        200: '请求成功',
        201: '新建或修改数据成功',
        202: '服务器已接受请求，但尚未处理',
        204: '删除数据成功',
        400: '请求参数有误',
        401: '用户没有权限（令牌、用户名、密码错误）',
        403: '用户得到授权，但是访问是被禁止的',
        404: '请求资源不存在',
        406: '请求的资源的内容特性无法满足请求头中的条件',
        408: '请求超时',
        410: '请求的资源被永久删除，且不会再得到的',
        422: '当创建一个对象时，发生一个验证错误',
        500: '服务器发生错误，请检查服务器',
        501: '服务未实现',
        502: '网络连接错误',
        503: '服务不可用，服务器暂时过载或维护',
        504: '网络连接超时',
        505: 'HTTP版本不受支持',
        2100: '会话已经失效（令牌失效）',
      };
      const message = codeMessage[status];
      return {
        success: false,
        code: status,
        data: null,
        message,
      };
    };

    this.controller = new AbortController();
  }
  async post(url: string, options: OptionType) {
    options = options ? options : defaultOptions;
    // const { headers, params } = options
    let result = null;

    const response: any = await this.request(url, {
      method: 'POST',
      ...options,
    });

    // 需要拷贝一个
    // const response_backup = response.clone()
    result = response.ok ? await response.json() : this.errorHandler(response);
    console.log(response);
    return result;
  }

  async get(url: string, options?: OptionType) {
    options = options ? options : defaultOptions;
    // const { headers, params } = options
    let result = null;

    const response: any = await this.request(url, {
      method: 'GET',
      ...options,
    });
    result = response.ok ? await response.json() : this.errorHandler(response);

    return result;
  }
  // 请求拦截
  async before(config: any) {
    return new Promise((resolve, reject) => {});
  }

  // 请求之后拦截
  async after(config: any) {
    return new Promise((resolve, reject) => {});
  }

  // 终止请求
  abort() {}

  // use
  async use() {}
}
