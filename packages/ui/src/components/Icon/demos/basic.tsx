import React, { useState, useEffect } from 'react';
import { Input, message } from 'antd';
import { Icon } from '@d-botao/ui';
import { jsonUrl } from '../index';
import styles from './style.less';
import Fetch from './fetch';

const request = new Fetch();

const getIconList = async () => {
  const result = await request.get(jsonUrl);
  return result.glyphs;
};

export default () => {
  // 搜索模块
  const [keyWord, setKeyWord] = useState('');
  const [list, setList] = useState([] as any[]);
  const filterList = list.filter((item) => {
    return item.name.includes(keyWord) || item.font_class.includes(keyWord);
  });
  const getList = async () => {
    const data = await getIconList();
    setList(data || []);
  };
  useEffect(() => {
    getList();
  }, [0]);
  // 复制模块
  const copyData = (e: any) => {
    const data = document.getElementById('textArea') as HTMLInputElement;
    data.defaultValue = e;
    data.select();
    const tag = document.execCommand('copy');

    if (tag) {
      message.success(`<${data.defaultValue}> copied`);
    } else {
      message.warning('copy failed');
    }
  };

  return (
    <>
      <div className={styles.search}>
        <Input
          className={styles.searchInput}
          type="text"
          placeholder="请输入关键字搜索"
          onChange={(e) => {
            setKeyWord(e.target.value);
          }}
        />
      </div>
      <textarea
        className={styles.textArea}
        id="textArea"
        style={{ height: 0, width: 0, opacity: 0, position: 'absolute' }}
      ></textarea>
      {filterList.map((item) => {
        return (
          <div
            className={styles.content}
            key={item.icon_id}
            onClick={() => {
              copyData(item.font_class);
            }}
          >
            <Icon className={styles.quarkicon} type={item.font_class} />

            <div className={styles.itemName}>{item.name}</div>

            <div className={styles.itemFontClass}>{item.font_class}</div>
          </div>
        );
      })}
    </>
  );
};
