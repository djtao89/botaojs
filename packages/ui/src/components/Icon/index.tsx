import { createFromIconfontCN } from '@ant-design/icons';

const scriptUrl = '//at.alicdn.com/t/font_1935092_afnb8v96wyg.js';

const Icon = createFromIconfontCN({
  scriptUrl,
});

export default Icon;

export const jsonUrl = scriptUrl.replace('.js', '.json');
