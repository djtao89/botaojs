---
order: 6
title: Icon - 基础图标
group:
  title: 基础功能组件
  path: /basicFun
nav:
  title: ui
  path: /ui
---

### 展示

例举一些常见的 icon 图标 <br/> 点击复制 icon 名称

<code src="./demos/basic.tsx" />
