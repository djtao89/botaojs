interface dataFilterParams {
    /**
     * 过滤函数
     */
    onFilter: (x: any) => boolean;
    /**
     * 过滤源数组
     */
    source: any[];
}
/**
 *@title 根据过滤函数返回一个新的过滤数组，保留树结构.
 */
export declare function dataFilter(props: dataFilterParams): any[];
declare type DeppParams = {
    /**
     * 子元素
     */
    children?: {
        [x: string]: any;
        children?: Record<string, any>[];
    }[];
    [x: string]: any;
}[];
export declare function filterTree(source: any[], onFilter: (x: any) => boolean): any[];
/**
 * @description 获取树的深度
 * @title 获取树的深度
 * @exports false
 */
export declare function sourceDeep(source: DeppParams): number;
interface DeepSourceToRowKeysProps {
    /**
     * 主键Id
     */
    rowKey: string;
    /**
     * 数据源
     */
    source: Record<string, any>[];
    /**
     * 遍历深度
     */
    deep: number;
}
/**
 * @description 根据deep深度获取树元素主键的集合
 * @title 根据deep深度获取树元素主键的集合
 * @exports false
 */
export declare function deppSourceToRowKeys({ rowKey, source, deep }: DeepSourceToRowKeysProps): string[];
interface FindRowByKeyProps {
    /**
     * 主键Id
     */
    rowKey: string;
    /**
     * 数据源
     */
    source: Record<string, any>[];
    /**
     * 寻找的key
     */
    key: string;
}
/**
 * @description 根据key寻找数组中的对数据
 * @title 根据key获取数据
 * @exports false
 */
export declare function findRowByKey({ key, source, rowKey }: FindRowByKeyProps): any;
interface moveRowProps {
    /**
     * 移动的row
     */
    movingRowKey: any;
    /**
     * 选中的row
     */
    targetRowKey: any;
    /**
     * 数据源
     */
    source: Record<string, any>[];
    /**
     * 主键
     */
    rowKey: string;
    /**
     * 移动方式
     */
    direction: 'up' | 'down' | 'in';
}
/**
 * @description 将数组中的某一个行移动到另一行的相对位置
 * @title 根据key获取数据
 * @exports false
 */
export declare function moveRow({ movingRowKey, targetRowKey, source, rowKey, direction }: moveRowProps): any[];
interface moveRowProProps {
    /**
     * 移动的row
     */
    movingRowKey: any;
    /**
     * 选中的row
     */
    targetRowKey: any;
    /**
     * 数据源
     */
    sources: Record<string, any>[][];
    /**
     * 主键
     */
    rowKey: string;
    /**
     * 移动方式
     */
    direction: 'up' | 'down' | 'in';
}
/**
 * @description 将多个数组中的某一个行移动到另一行的相对位置
 * @title 根据key获取数据
 * @exports false
 */
export declare function moveRowPro({ movingRowKey, targetRowKey, sources, rowKey, direction, }: moveRowProProps): any[];
interface refreshRowInSourceProps {
    row: any;
    source: any[];
    rowKey: string;
}
/**
 * @description 更新dataSource中的某行
 * @title 更新某行
 * @export false
 */
export declare function refreshRowInSource({ row, source, rowKey }: refreshRowInSourceProps): any[];
interface treeForEachProps {
    /**
     * 处理函数
     */
    handler: (row: any, index: number, deep?: number) => void;
    /**
     * 数据源
     */
    source: any[];
    /**
     * 当前遍历的深度
     */
    deep?: number;
}
/**
 * @description 遍历树
 * @title 遍历树
 * @export false
 */
export declare function treeForEach({ handler, source, deep }: treeForEachProps): void;
interface treeMapProps {
    /**
     * 处理函数
     */
    handler: (row: any, index: number, deep?: number) => any;
    /**
     * 数据源
     */
    source: any[];
    /**
     * 当前遍历的深度
     */
    deep?: number;
}
/**
 * @description 遍历树
 * @title 遍历树
 * @export false
 */
export declare function treeMap({ handler, source, deep }: treeMapProps): any[];
export declare const getNodeString: (node: any) => string | number | boolean;
export declare function arrayTreeFilter<T>(data: T[], filterFn: (item: T, level: number) => boolean, options?: {
    childrenKeyName?: string;
}): T[];
/**
 * @description 获取树形所有孩子节点
 * @title 获取树形所有孩子节点
 * @export []
 */
export declare function getAllChildrens(nodes: any[]): any[];
/**
 * @description 树拍平 树转数组
 * @title 树拍平 树转数组
 * @export []
 */
export declare function flatTree(nodes: any[]): any[];
/**
 * 节点路径查找
 *
 * @param {any[]} tree 数据源
 * @param {*} func 过滤条件
 * @param {(string | number)} dataIndex 主键
 * @param {string[]} [path=[]] 可不填
 * @return {*}  {string[]}
 */
export declare const treeFindPath: (tree: any[], func: any, dataIndex: string | number, path?: string[]) => string[] | number[];
export {};
