import { cloneDeep, merge } from 'lodash';

var isArray = function isArray(value) {
  return Object.prototype.toString.call(value) === '[object Array]';
};

var isBoolean = function isBoolean(value) {
  return Object.prototype.toString.call(value) === '[object Boolean]';
};

var isBigInt = function isBigInt(value) {
  return Object.prototype.toString.call(value) === '[object BigInt]';
};

var isDate = function isDate(value) {
  return Object.prototype.toString.call(value) === '[object Date]';
};

var isFunction = function isFunction(value) {
  return Object.prototype.toString.call(value) === '[object Function]';
};

var isNull = function isNull(value) {
  return Object.prototype.toString.call(value) === '[object Null]';
};

var isNumber = function isNumber(value) {
  return Object.prototype.toString.call(value) === '[object Number]';
};

var isObject = function isObject(value) {
  return Object.prototype.toString.call(value) === '[object Object]';
};

var isUndefined = function isUndefined(value) {
  return Object.prototype.toString.call(value) === '[object Undefined]';
};

var isString = function isString(value) {
  return Object.prototype.toString.call(value) === '[object String]';
};

var isSymbol = function isSymbol(value) {
  return Object.prototype.toString.call(value) === '[object Symbol]';
};

export default {
  isArray: isArray,
  isBoolean: isBoolean,
  isBigInt: isBigInt,
  isDate: isDate,
  isNull: isNull,
  isNumber: isNumber,
  isObject: isObject,
  isFunction: isFunction,
  isUndefined: isUndefined,
  isString: isString,
  isSymbol: isSymbol,
  cloneDeep: cloneDeep,
  merge: merge
};