export const CODE_MESSAGE: any = {
  200: '请求成功',
  201: '新建或修改数据成功',
  202: '服务器已接受请求，但尚未处理',
  204: '删除数据成功',
  400: '请求参数有误',
  401: '用户没有权限（令牌、用户名、密码错误）',
  403: '用户得到授权，但是访问是被禁止的',
  404: '请求资源不存在',
  406: '请求的资源的内容特性无法满足请求头中的条件',
  408: '请求超时',
  410: '请求的资源被永久删除，且不会再得到的',
  422: '当创建一个对象时，发生一个验证错误',
  500: '服务器发生错误，请检查服务器',
  501: '服务未实现',
  502: '网络连接错误',
  503: '服务不可用，服务器暂时过载或维护',
  504: '网络连接超时',
  505: 'HTTP版本不受支持',
  2100: '会话已经失效（令牌失效）',
};

// content-type
export const CONTENT_TYPE = {
  JSON: 'application/json; charset=UTF-8',
  FORM: 'application/x-www-form-urlencoded; charset=UTF-8',
  FORM_DATA: 'multipart/form-data; charset=UTF-8',
};

// Http method
export const HTTP_METHODS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  PATCH: 'PATCH',
  DELETE: 'DELETE',
};
