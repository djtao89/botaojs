import React from 'react';
import { Button } from 'antd';
import { request } from '@d-botao/utils';

export default () => {
  // const request = new Request({ lib: 'fetch' });

  const useGET = async () => {
    // console.log(request);

    const res = await request('/api/users/get', {
      method: 'get',
      params: {
        get: 1,
      },
    });

    console.log(res);
  };

  const usePOST = async () => {
    // console.log(request);

    const res = await request('/api/users/post', {
      method: 'post',
      data: {
        post: 1,
      },
    });

    console.log(res);
  };
  return (
    <>
      <Button onClick={usePOST}>use POST</Button>
      <br />
      <br />
      <Button onClick={useGET}>use GET</Button>
      <br />
      <br />
      <Button>use umi-request</Button>
      <br />
    </>
  );
};
