import requestMsgBox from './requestMsgBox';
import { Msgbox } from '@d-botao/ui';

export default (error: any) => {
  console.log(error);
  const { response, type, request } = error;

  if (type === 'Timeout') {
    Msgbox.error({
      title: '网络连接超时',
      content: '网络连接超时',
    });
    return error;
  }
  if (response && response.status) {
    const { status } = response;
    requestMsgBox({
      code: response.status,
      msg: `请求错误 ${status}:errorText `,
    });
  } else if (!response) {
    requestMsgBox({
      code: 'error',
      msg: '网络异常,您的网络发生异常，无法连接服务器',
    });
  }

  return response;
};
