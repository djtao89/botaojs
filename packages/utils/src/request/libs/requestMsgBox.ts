import { Msgbox } from '@d-botao/ui';
import { msgToHtml } from './msgToHtml';

const errorMap = new Map();

export default ({ code = 'error', msg = '未知错误' }) => {
  const mapKey = `${code}_${msg}`;
  console.log(mapKey);
  if (errorMap.has(mapKey)) {
    return;
  }
  errorMap.set(mapKey, new Date().getTime());
  if (code === 'warn') {
    Msgbox.warning({
      message: msgToHtml(msg),
      onClose: () => {
        errorMap.delete(mapKey);
      },
    });
  } else if (code === 'info') {
    Msgbox.info({
      message: msgToHtml(msg),
      onClose: () => {
        errorMap.delete(mapKey);
      },
    });
  } else {
    Msgbox.error({
      message: msgToHtml(msg),
      onClose: () => {
        errorMap.delete(mapKey);
      },
    });
  }
};
