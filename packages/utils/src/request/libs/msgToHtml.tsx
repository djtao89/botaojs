import React from 'react';

export const msgToHtml = (msg = '') => {
  const msgArray = msg.split('<br/>').filter((item) => item && item !== '');
  return (
    <div style={{ display: 'inline-block' }}>
      {msgArray.map((item, index) => {
        return (
          <>
            {item}
            {index !== msgArray.length && <br />}
          </>
        );
      })}
    </div>
  );
};
