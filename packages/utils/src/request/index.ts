/* eslint-disable complexity */
/**
 * request 网络请求工具
 * 更详细的 api 文档: https://github.com/umijs/umi-request
 */
// import { msgToHtml } from './msgToHtml';
// import { history } from 'umi';
import { extend } from 'umi-request';
import { Msgbox } from '@d-botao/ui';
import { CODE_MESSAGE, CONTENT_TYPE } from './constants';
import requestMsgBox from './libs/requestMsgBox';
import errorHandler from './libs/errorHandler';

// 请求异常弹窗多个一样的仅仅显示一个

/**
 * 配置request请求时的默认参数
 */
const fetchInstance = extend({
  // 默认错误处理
  errorHandler,
  // charset: 'utf-8',
  /** 允许携带cookies */
  credentials: 'include',
  /** 允许跨域 */
  mode: 'cors',
  timeout: 60000,
  cache: 'no-cache',
  headers: {
    Accept: CONTENT_TYPE.JSON,
    'Content-Type': CONTENT_TYPE.JSON,
  },
});

/**
 * 请求拦截器
 */
fetchInstance.interceptors.request.use((url, options) => {
  const config: any = { ...options };

  if (options['Content-Type']) {
    config.headers['Content-Type'] = options['Content-Type'];
  }

  return { url, options };
});

/**
 * 响应拦截器
 */
fetchInstance.interceptors.response.use(async (response, options) => {
  // const contentType: any = response.headers.get('Content-Type');

  const data = await response.clone().json();

  const { showMsg = true }: any = options;

  const code = data.code.toString().toUpperCase();
  const message = data.message || data.msg;

  // const { showConfitm = false }: any = options;
  // token无效，则跳转到登录页面
  if (code === '2100') {
    return response;
  } else if (code === '201') {
    Msgbox.success({ title: '信息', content: message });
  } else if (code !== 'SUCCESS' && code !== '200') {
    if (showMsg) {
      requestMsgBox({
        code: data.code || data.status,
        msg: message || CODE_MESSAGE[code || data.status],
      });
    }
    return data;
  }

  return data;
});

export default fetchInstance;
