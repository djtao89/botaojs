---
order: 0
title: BeanUtil - 数据过滤类
group:
  path: /utils
nav:
  title: 工具类
  path: /utils
  order: 20
---

### 数据过滤类

- 基本判断类型有isArray、isBoolean、isBigInt、isDate等
- 用法：BeanUtil.isArray(value)判断是否是数组
<code src="./demos/index.tsx">


### deepclone(深拷贝)

- 参数：cloneDeep(value): 要深拷贝的值。
<code src="./demos/deepclone">


### merge(合并)

- 参数：merge(a，b, c)要合并的值。
<code src="./demos/merge">



