import React from 'react';
import { BeanUtil } from '@d-botao/utils';

export default () => {
  const value = 'zyf';
  return (
    <div className="textcolor">
      value：{value}
      <div>
        <br />
        isArray：{BeanUtil.isNumber(value).toString()}
      </div>
      <br />
      <div>isBoolean：{BeanUtil.isBoolean(value).toString()}</div>
      <br />
      <div>isBigInt：{BeanUtil.isBigInt(value).toString()}</div>
      <br />
      <div>isDate：{BeanUtil.isDate(value).toString()}</div>
      <br />
      <div>isNull：{BeanUtil.isNull(value).toString()}</div>
      <br />
      <div>isNumber：{BeanUtil.isNumber(value).toString()}</div>
      <br />
      <div>isObject：{BeanUtil.isObject(value).toString()}</div>
      <br />
      <div>isFunction：{BeanUtil.isFunction(value).toString()}</div>
      <br />
      <div>isUndefined：{BeanUtil.isUndefined(value).toString()}</div>
      <br />
      <div>isString：{BeanUtil.isString(value).toString()}</div>
    </div>
  );
};
