import React from 'react';
import { BeanUtil } from '@d-botao/utils';

export default () => {
  const a = { pro: { obj: 2 } };
  const b1 = a; // 浅拷贝
  const b2 = BeanUtil.cloneDeep(a); // 深拷贝
  return (
    <div
      style={{
        color: 'green',
      }}
    >
      <p>a.pro：{JSON.stringify(a.pro)}</p>
      <p>b1.pro：{JSON.stringify(b1.pro)}</p>
      <p>b2.pro：{JSON.stringify(b2.pro)}</p>
      <p>a.pro === b1.pro：{(a.pro === b1.pro).toString()}</p>
      <p>a.pro === b2.pro：{(a.pro === b2.pro).toString()}</p>
    </div>
  );
};
