import { BeanUtil } from '@d-botao/utils';
import React from 'react';

export default () => {
  const a = { name: 'zyf' };
  const b = { sex: 'male' };
  return (
    <div className="textcolor">
      <p>a：{JSON.stringify(a)}</p>
      <p>b：{JSON.stringify(b)}</p>
      <p>c = merge(a,b)</p>
      <div>
        <span>c：</span>
        {JSON.stringify(BeanUtil.merge(a, b))}
      </div>
    </div>
  );
};
