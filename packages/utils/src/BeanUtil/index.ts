import { cloneDeep, merge } from 'lodash';

const isArray = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Array]';
};
const isBoolean = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Boolean]';
};
const isBigInt = (value: any) => {
  return Object.prototype.toString.call(value) === '[object BigInt]';
};
const isDate = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Date]';
};
const isFunction = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Function]';
};
const isNull = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Null]';
};
const isNumber = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Number]';
};
const isObject = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Object]';
};
const isUndefined = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Undefined]';
};
const isString = (value: any) => {
  return Object.prototype.toString.call(value) === '[object String]';
};
const isSymbol = (value: any) => {
  return Object.prototype.toString.call(value) === '[object Symbol]';
};

export default {
  isArray,
  isBoolean,
  isBigInt,
  isDate,
  isNull,
  isNumber,
  isObject,
  isFunction,
  isUndefined,
  isString,
  isSymbol,
  cloneDeep,
  merge,
};
