/* eslint-disable no-param-reassign */
import BeanUtil from '../BeanUtil';

interface dataFilterParams {
  /**
   * 过滤函数
   */
  onFilter: (x: any) => boolean;
  /**
   * 过滤源数组
   */
  source: any[];
}

/**
 *@title 根据过滤函数返回一个新的过滤数组，保留树结构.
 */
export function dataFilter(props: dataFilterParams) {
  /**
   *@description 根据过滤函数返回一个新的过滤数组，保留树结构.
   */
  const { onFilter, source } = props;
  const newSource = BeanUtil.cloneDeep(source);
  return newSource.filter((item: { children?: any[] }) => {
    if (onFilter(item)) {
      if (item.children?.length) {
        // eslint-disable-next-line no-param-reassign
        item.children = dataFilter({
          onFilter,
          source: item.children,
        });
      }
      return true;
    }
    if (item.children?.length) {
      // eslint-disable-next-line no-param-reassign
      item.children = dataFilter({
        onFilter,
        source: item.children,
      });
      if (item.children?.length) {
        return true;
      }
    }
    return false;
  });
}

type DeppParams = {
  /**
   * 子元素
   */
  children?: {
    [x: string]: any;
    children?: Record<string, any>[];
  }[];
  [x: string]: any;
}[];

export function filterTree(source: any[], onFilter: (x: any) => boolean) {
  /**
   *@description 根据过滤函数返回一个新的过滤数组，保留树结构,相比于dataFilter不保留父节点
   */
  return source
    .filter((item: any) => {
      return onFilter(item);
    })
    .map((myItem: any) => {
      myItem = { ...myItem };
      if (myItem.children) {
        myItem.children = filterTree(myItem.children, onFilter);
      }
      return myItem;
    });
}

/**
 * @description 获取树的深度
 * @title 获取树的深度
 * @exports false
 */
export function sourceDeep(source: DeppParams) {
  if (!source) {
    return 0;
  }
  const deeps = [0];
  source.forEach((item) => {
    if (item.children?.length) {
      deeps.push(sourceDeep(item.children));
    }
  });
  return 1 + Math.max(...deeps);
}

interface DeepSourceToRowKeysProps {
  /**
   * 主键Id
   */
  rowKey: string;
  /**
   * 数据源
   */
  source: Record<string, any>[];
  /**
   * 遍历深度
   */
  deep: number;
}
/**
 * @description 根据deep深度获取树元素主键的集合
 * @title 根据deep深度获取树元素主键的集合
 * @exports false
 */
export function deppSourceToRowKeys({ rowKey, source = [], deep = 0 }: DeepSourceToRowKeysProps) {
  if (deep <= 0) {
    return [];
  }
  const keys: string[] = [];
  source.forEach((item) => {
    if (item[rowKey]) {
      keys.push(item[rowKey]);
    }
    if (item.children?.length) {
      keys.push(
        ...deppSourceToRowKeys({
          source: item.children,
          rowKey,
          deep: deep - 1,
        }),
      );
    }
  });
  return keys;
}

interface FindRowByKeyProps {
  /**
   * 主键Id
   */
  rowKey: string;
  /**
   * 数据源
   */
  source: Record<string, any>[];
  /**
   * 寻找的key
   */
  key: string;
}
/**
 * @description 根据key寻找数组中的对数据
 * @title 根据key获取数据
 * @exports false
 */
export function findRowByKey({ key, source, rowKey }: FindRowByKeyProps) {
  const rows = source;
  for (let i = 0; i < rows.length; i += 1) {
    const row = rows[i];
    if (row[rowKey] === key) {
      return row;
    }
    if (row.children) {
      const childRow: any = findRowByKey({
        key,
        source: row.children,
        rowKey,
      });
      if (childRow) {
        return childRow;
      }
    }
  }
  return null;
}

interface moveRowProps {
  /**
   * 移动的row
   */
  movingRowKey: any;
  /**
   * 选中的row
   */
  targetRowKey: any;
  /**
   * 数据源
   */
  source: Record<string, any>[];
  /**
   * 主键
   */
  rowKey: string;
  /**
   * 移动方式
   */
  direction: 'up' | 'down' | 'in';
}
/**
 * @description 将数组中的某一个行移动到另一行的相对位置
 * @title 根据key获取数据
 * @exports false
 */
export function moveRow({ movingRowKey, targetRowKey, source, rowKey, direction }: moveRowProps) {
  if (movingRowKey === targetRowKey) {
    return source;
  }
  const movingRow = findRowByKey({
    rowKey,
    key: movingRowKey,
    source,
  });
  if (!movingRow) {
    return source;
  }
  // 把父节点移动到自己的子节点中属于非法操作
  if (movingRow.children) {
    if (
      findRowByKey({
        rowKey,
        key: targetRowKey,
        source: movingRow.children,
      })
    ) {
      return source;
    }
  }
  type sourceFc = (x: any[]) => any[];
  const deleteRows: sourceFc = (targetSource: any[]) => {
    return targetSource
      .map((item: any) => {
        if (item[rowKey] === movingRowKey) {
          return null;
        }
        if (item.children) {
          item.children = deleteRows(item.children);
        }
        return item;
      })
      .filter((item) => item);
  };
  const addSource: sourceFc = (targetSource: any[]) => {
    return targetSource
      .map((item: any) => {
        if (item[rowKey] === targetRowKey) {
          if (direction === 'up') {
            return [movingRow, item];
          }
          if (direction === 'down') {
            return [item, movingRow];
          }
          if (direction === 'in') {
            if (!item.children) {
              item.children = [];
            }
            item.children.push(BeanUtil.cloneDeep(movingRow));
            // 为了清除symbol值 不得不在此做一次循环
            const newItem: any = {};
            Object.keys(item).forEach((key) => {
              newItem[key] = item[key];
            });
            return newItem;
          }
        }
        if (item.children) {
          item.children = addSource(item.children);
        }
        return item;
      })
      .flat();
  };
  return addSource(deleteRows(BeanUtil.cloneDeep(source)));
}

interface moveRowProProps {
  /**
   * 移动的row
   */
  movingRowKey: any;
  /**
   * 选中的row
   */
  targetRowKey: any;
  /**
   * 数据源
   */
  sources: Record<string, any>[][];
  /**
   * 主键
   */
  rowKey: string;
  /**
   * 移动方式
   */
  direction: 'up' | 'down' | 'in';
}
/**
 * @description 将多个数组中的某一个行移动到另一行的相对位置
 * @title 根据key获取数据
 * @exports false
 */
export function moveRowPro({
  movingRowKey,
  targetRowKey,
  sources,
  rowKey,
  direction,
}: moveRowProProps) {
  const sourcesToTree = sources.map((source) => {
    const item = {} as Record<string, any>;
    item[rowKey] = Math.random();
    item.children = source;
    return item;
  });
  const movedSourcesTree = moveRow({
    movingRowKey,
    targetRowKey,
    source: sourcesToTree,
    rowKey,
    direction,
  });
  return movedSourcesTree.map((item) => item.children);
}

interface refreshRowInSourceProps {
  row: any;
  source: any[];
  rowKey: string;
}

/**
 * @description 更新dataSource中的某行
 * @title 更新某行
 * @export false
 */
export function refreshRowInSource({ row, source, rowKey }: refreshRowInSourceProps): any[] {
  return source.map((item) => {
    if (item[rowKey] === row[rowKey]) {
      return {
        ...item,
        ...row,
      };
    }
    if (item.children) {
      return {
        ...item,
        children: refreshRowInSource({
          row,
          source: item.children,
          rowKey,
        }),
      };
    }
    return item;
  });
}

interface treeForEachProps {
  /**
   * 处理函数
   */
  handler: (row: any, index: number, deep?: number) => void;
  /**
   * 数据源
   */
  source: any[];
  /**
   * 当前遍历的深度
   */
  deep?: number;
}
/**
 * @description 遍历树
 * @title 遍历树
 * @export false
 */
export function treeForEach({ handler, source, deep = 0 }: treeForEachProps) {
  source.forEach((item: any, index: number) => {
    handler(item, index, deep);
    if (item.children) {
      treeForEach({
        source: item.children,
        handler,
        deep: deep + 1,
      });
    }
  });
}

interface treeMapProps {
  /**
   * 处理函数
   */
  handler: (row: any, index: number, deep?: number) => any;
  /**
   * 数据源
   */
  source: any[];
  /**
   * 当前遍历的深度
   */
  deep?: number;
}
/**
 * @description 遍历树
 * @title 遍历树
 * @export false
 */
export function treeMap({ handler, source, deep = 0 }: treeMapProps) {
  return source.map((item: any, index: number) => {
    const custom = {} as Record<any, any>;
    const handledData = handler(item, index, deep);
    if (handledData && handledData.children) {
      custom.children = treeMap({
        source: handledData.children,
        handler,
        deep,
      }).filter((child) => !!child);
      return {
        ...handledData,
        ...custom,
      };
    }
    return handledData;
  });
}

export const getNodeString = (node: any) => {
  if (typeof node === 'string' || typeof node === 'number' || typeof node === 'boolean') {
    return node;
  }
  if (node === undefined || node === null) return '';
  if (typeof node === 'object') {
    let str = '';
    const { children } = node.props || {};
    if (children instanceof Array) {
      children.forEach((item) => {
        str += getNodeString(item);
      });
    }
    str += getNodeString(children);
    return str;
  }
  return '';
};

export function arrayTreeFilter<T>(
  data: T[],
  filterFn: (item: T, level: number) => boolean,
  options?: {
    childrenKeyName?: string;
  },
) {
  options = options || {};
  options.childrenKeyName = options.childrenKeyName || 'children';
  let children = data || [];
  const result: T[] = [];
  let level = 0;
  do {
    // eslint-disable-next-line @typescript-eslint/no-loop-func
    const foundItem: T = children.filter((item: any) => filterFn(item, level))[0];
    if (!foundItem) {
      break;
    }
    result.push(foundItem);
    children = (foundItem as any)[options.childrenKeyName] || [];
    level += 1;
  } while (children.length > 0);
  return result;
}

/**
 * @description 获取树形所有孩子节点
 * @title 获取树形所有孩子节点
 * @export []
 */
export function getAllChildrens(nodes: any[]) {
  const childNodes: any[] = [];
  nodes.forEach((item: any) => {
    if (item.children?.length) {
      childNodes.push(...getAllChildrens(item.children));
    } else {
      childNodes.push(item);
    }
  });
  return childNodes;
}

/**
 * @description 树拍平 树转数组
 * @title 树拍平 树转数组
 * @export []
 */
export function flatTree(nodes: any[]) {
  let result: any[] = [];
  nodes.forEach((node: any) => {
    result.push(node);
    if (Array.isArray(node.children)) {
      result = result.concat(flatTree(node.children));
    }
  });
  return result;
}

/**
 * 节点路径查找
 *
 * @param {any[]} tree 数据源
 * @param {*} func 过滤条件
 * @param {(string | number)} dataIndex 主键
 * @param {string[]} [path=[]] 可不填
 * @return {*}  {string[]}
 */
export const treeFindPath = (
  tree: any[],
  func: any,
  dataIndex: string | number,
  path: string[] = [],
): string[] | number[] => {
  if (!tree) return [];
  for (const data of tree) {
    path.push(data[dataIndex]);
    if (func(data)) return path;
    if (data.children) {
      const findChildren = treeFindPath(data.children, func, dataIndex, path);
      if (findChildren.length) return findChildren;
    }
    path.pop();
  }
  return [];
};
