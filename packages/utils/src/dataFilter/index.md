---
order: 0
title: dataFilter - 数据过滤类
group:
  path: /utils
nav:
  title: 工具类
  path: /utils
---

### dataFilter

根据过滤函数生成一个新数组

<!-- <code src="./demos/index.tsx"> -->

### sourceDeep

获取树的深度

<!-- <code src="./demos/sourceDeep.tsx"> -->

### deppSourceToRowKeys

根据 deep 深度获取树元素主键的集合

<!-- <code src="./demos/deppSourceToRowKeys.tsx"> -->

### findRowByKey

根据 key 寻找数组中的对数据

<!-- <code src="./demos/findRowByKey.tsx"> -->

### moveRow

将数组中的某一个行移动到另一行的相对位置

<!-- <code src="./demos/moveRow.tsx"> -->

### refreshRowInSource

更新 dataSource 中的某行

<!-- <code src="./demos/refreshRowInSource.tsx"> -->

### treeFindPath

查找某个节点的路径

<!-- <code src="./demos/findPath.tsx"> -->

### getNodeString

获取某个 react 元素的字符串值

<!-- <code src="./demos/nodeToString.tsx"> -->
