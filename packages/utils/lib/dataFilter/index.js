"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.arrayTreeFilter = arrayTreeFilter;
exports.dataFilter = dataFilter;
exports.deppSourceToRowKeys = deppSourceToRowKeys;
exports.filterTree = filterTree;
exports.findRowByKey = findRowByKey;
exports.flatTree = flatTree;
exports.getAllChildrens = getAllChildrens;
exports.getNodeString = void 0;
exports.moveRow = moveRow;
exports.moveRowPro = moveRowPro;
exports.refreshRowInSource = refreshRowInSource;
exports.sourceDeep = sourceDeep;
exports.treeFindPath = void 0;
exports.treeForEach = treeForEach;
exports.treeMap = treeMap;

var _BeanUtil = _interopRequireDefault(require("../BeanUtil"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 *@title 根据过滤函数返回一个新的过滤数组，保留树结构.
 */
function dataFilter(props) {
  /**
   *@description 根据过滤函数返回一个新的过滤数组，保留树结构.
   */
  var onFilter = props.onFilter,
      source = props.source;

  var newSource = _BeanUtil.default.cloneDeep(source);

  return newSource.filter(function (item) {
    var _item$children2;

    if (onFilter(item)) {
      var _item$children;

      if ((_item$children = item.children) === null || _item$children === void 0 ? void 0 : _item$children.length) {
        // eslint-disable-next-line no-param-reassign
        item.children = dataFilter({
          onFilter: onFilter,
          source: item.children
        });
      }

      return true;
    }

    if ((_item$children2 = item.children) === null || _item$children2 === void 0 ? void 0 : _item$children2.length) {
      var _item$children3;

      // eslint-disable-next-line no-param-reassign
      item.children = dataFilter({
        onFilter: onFilter,
        source: item.children
      });

      if ((_item$children3 = item.children) === null || _item$children3 === void 0 ? void 0 : _item$children3.length) {
        return true;
      }
    }

    return false;
  });
}

function filterTree(source, onFilter) {
  /**
   *@description 根据过滤函数返回一个新的过滤数组，保留树结构,相比于dataFilter不保留父节点
   */
  return source.filter(function (item) {
    return onFilter(item);
  }).map(function (myItem) {
    myItem = _objectSpread({}, myItem);

    if (myItem.children) {
      myItem.children = filterTree(myItem.children, onFilter);
    }

    return myItem;
  });
}
/**
 * @description 获取树的深度
 * @title 获取树的深度
 * @exports false
 */


function sourceDeep(source) {
  if (!source) {
    return 0;
  }

  var deeps = [0];
  source.forEach(function (item) {
    var _item$children4;

    if ((_item$children4 = item.children) === null || _item$children4 === void 0 ? void 0 : _item$children4.length) {
      deeps.push(sourceDeep(item.children));
    }
  });
  return 1 + Math.max.apply(Math, deeps);
}
/**
 * @description 根据deep深度获取树元素主键的集合
 * @title 根据deep深度获取树元素主键的集合
 * @exports false
 */


function deppSourceToRowKeys(_ref) {
  var rowKey = _ref.rowKey,
      _ref$source = _ref.source,
      source = _ref$source === void 0 ? [] : _ref$source,
      _ref$deep = _ref.deep,
      deep = _ref$deep === void 0 ? 0 : _ref$deep;

  if (deep <= 0) {
    return [];
  }

  var keys = [];
  source.forEach(function (item) {
    var _item$children5;

    if (item[rowKey]) {
      keys.push(item[rowKey]);
    }

    if ((_item$children5 = item.children) === null || _item$children5 === void 0 ? void 0 : _item$children5.length) {
      keys.push.apply(keys, _toConsumableArray(deppSourceToRowKeys({
        source: item.children,
        rowKey: rowKey,
        deep: deep - 1
      })));
    }
  });
  return keys;
}
/**
 * @description 根据key寻找数组中的对数据
 * @title 根据key获取数据
 * @exports false
 */


function findRowByKey(_ref2) {
  var key = _ref2.key,
      source = _ref2.source,
      rowKey = _ref2.rowKey;
  var rows = source;

  for (var i = 0; i < rows.length; i += 1) {
    var row = rows[i];

    if (row[rowKey] === key) {
      return row;
    }

    if (row.children) {
      var childRow = findRowByKey({
        key: key,
        source: row.children,
        rowKey: rowKey
      });

      if (childRow) {
        return childRow;
      }
    }
  }

  return null;
}
/**
 * @description 将数组中的某一个行移动到另一行的相对位置
 * @title 根据key获取数据
 * @exports false
 */


function moveRow(_ref3) {
  var movingRowKey = _ref3.movingRowKey,
      targetRowKey = _ref3.targetRowKey,
      source = _ref3.source,
      rowKey = _ref3.rowKey,
      direction = _ref3.direction;

  if (movingRowKey === targetRowKey) {
    return source;
  }

  var movingRow = findRowByKey({
    rowKey: rowKey,
    key: movingRowKey,
    source: source
  });

  if (!movingRow) {
    return source;
  } // 把父节点移动到自己的子节点中属于非法操作


  if (movingRow.children) {
    if (findRowByKey({
      rowKey: rowKey,
      key: targetRowKey,
      source: movingRow.children
    })) {
      return source;
    }
  }

  var deleteRows = function deleteRows(targetSource) {
    return targetSource.map(function (item) {
      if (item[rowKey] === movingRowKey) {
        return null;
      }

      if (item.children) {
        item.children = deleteRows(item.children);
      }

      return item;
    }).filter(function (item) {
      return item;
    });
  };

  var addSource = function addSource(targetSource) {
    return targetSource.map(function (item) {
      if (item[rowKey] === targetRowKey) {
        if (direction === 'up') {
          return [movingRow, item];
        }

        if (direction === 'down') {
          return [item, movingRow];
        }

        if (direction === 'in') {
          if (!item.children) {
            item.children = [];
          }

          item.children.push(_BeanUtil.default.cloneDeep(movingRow)); // 为了清除symbol值 不得不在此做一次循环

          var newItem = {};
          Object.keys(item).forEach(function (key) {
            newItem[key] = item[key];
          });
          return newItem;
        }
      }

      if (item.children) {
        item.children = addSource(item.children);
      }

      return item;
    }).flat();
  };

  return addSource(deleteRows(_BeanUtil.default.cloneDeep(source)));
}
/**
 * @description 将多个数组中的某一个行移动到另一行的相对位置
 * @title 根据key获取数据
 * @exports false
 */


function moveRowPro(_ref4) {
  var movingRowKey = _ref4.movingRowKey,
      targetRowKey = _ref4.targetRowKey,
      sources = _ref4.sources,
      rowKey = _ref4.rowKey,
      direction = _ref4.direction;
  var sourcesToTree = sources.map(function (source) {
    var item = {};
    item[rowKey] = Math.random();
    item.children = source;
    return item;
  });
  var movedSourcesTree = moveRow({
    movingRowKey: movingRowKey,
    targetRowKey: targetRowKey,
    source: sourcesToTree,
    rowKey: rowKey,
    direction: direction
  });
  return movedSourcesTree.map(function (item) {
    return item.children;
  });
}
/**
 * @description 更新dataSource中的某行
 * @title 更新某行
 * @export false
 */


function refreshRowInSource(_ref5) {
  var row = _ref5.row,
      source = _ref5.source,
      rowKey = _ref5.rowKey;
  return source.map(function (item) {
    if (item[rowKey] === row[rowKey]) {
      return _objectSpread(_objectSpread({}, item), row);
    }

    if (item.children) {
      return _objectSpread(_objectSpread({}, item), {}, {
        children: refreshRowInSource({
          row: row,
          source: item.children,
          rowKey: rowKey
        })
      });
    }

    return item;
  });
}
/**
 * @description 遍历树
 * @title 遍历树
 * @export false
 */


function treeForEach(_ref6) {
  var handler = _ref6.handler,
      source = _ref6.source,
      _ref6$deep = _ref6.deep,
      deep = _ref6$deep === void 0 ? 0 : _ref6$deep;
  source.forEach(function (item, index) {
    handler(item, index, deep);

    if (item.children) {
      treeForEach({
        source: item.children,
        handler: handler,
        deep: deep + 1
      });
    }
  });
}
/**
 * @description 遍历树
 * @title 遍历树
 * @export false
 */


function treeMap(_ref7) {
  var handler = _ref7.handler,
      source = _ref7.source,
      _ref7$deep = _ref7.deep,
      deep = _ref7$deep === void 0 ? 0 : _ref7$deep;
  return source.map(function (item, index) {
    var custom = {};
    var handledData = handler(item, index, deep);

    if (handledData && handledData.children) {
      custom.children = treeMap({
        source: handledData.children,
        handler: handler,
        deep: deep
      }).filter(function (child) {
        return !!child;
      });
      return _objectSpread(_objectSpread({}, handledData), custom);
    }

    return handledData;
  });
}

var getNodeString = function getNodeString(node) {
  if (typeof node === 'string' || typeof node === 'number' || typeof node === 'boolean') {
    return node;
  }

  if (node === undefined || node === null) return '';

  if (_typeof(node) === 'object') {
    var str = '';

    var _ref8 = node.props || {},
        children = _ref8.children;

    if (children instanceof Array) {
      children.forEach(function (item) {
        str += getNodeString(item);
      });
    }

    str += getNodeString(children);
    return str;
  }

  return '';
};

exports.getNodeString = getNodeString;

function arrayTreeFilter(data, filterFn, options) {
  options = options || {};
  options.childrenKeyName = options.childrenKeyName || 'children';
  var children = data || [];
  var result = [];
  var level = 0;

  do {
    // eslint-disable-next-line @typescript-eslint/no-loop-func
    var foundItem = children.filter(function (item) {
      return filterFn(item, level);
    })[0];

    if (!foundItem) {
      break;
    }

    result.push(foundItem);
    children = foundItem[options.childrenKeyName] || [];
    level += 1;
  } while (children.length > 0);

  return result;
}
/**
 * @description 获取树形所有孩子节点
 * @title 获取树形所有孩子节点
 * @export []
 */


function getAllChildrens(nodes) {
  var childNodes = [];
  nodes.forEach(function (item) {
    var _item$children6;

    if ((_item$children6 = item.children) === null || _item$children6 === void 0 ? void 0 : _item$children6.length) {
      childNodes.push.apply(childNodes, _toConsumableArray(getAllChildrens(item.children)));
    } else {
      childNodes.push(item);
    }
  });
  return childNodes;
}
/**
 * @description 树拍平 树转数组
 * @title 树拍平 树转数组
 * @export []
 */


function flatTree(nodes) {
  var result = [];
  nodes.forEach(function (node) {
    result.push(node);

    if (Array.isArray(node.children)) {
      result = result.concat(flatTree(node.children));
    }
  });
  return result;
}
/**
 * 节点路径查找
 *
 * @param {any[]} tree 数据源
 * @param {*} func 过滤条件
 * @param {(string | number)} dataIndex 主键
 * @param {string[]} [path=[]] 可不填
 * @return {*}  {string[]}
 */


var treeFindPath = function treeFindPath(tree, func, dataIndex) {
  var path = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
  if (!tree) return [];

  var _iterator = _createForOfIteratorHelper(tree),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var data = _step.value;
      path.push(data[dataIndex]);
      if (func(data)) return path;

      if (data.children) {
        var findChildren = treeFindPath(data.children, func, dataIndex, path);
        if (findChildren.length) return findChildren;
      }

      path.pop();
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return [];
};

exports.treeFindPath = treeFindPath;