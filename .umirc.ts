import { readdirSync } from 'fs';
import { join } from 'path';
import { defineConfig } from 'dumi';
import modifyVars from './modifyVars';

// console.log(modifyVars);

const headPkgList = [];
const pkgList = readdirSync(join(__dirname, 'packages')).filter(
  (pkg) => pkg.charAt(0) !== '.' && !headPkgList.includes(pkg),
);

const alias = pkgList.reduce((pre, pkg) => {
  pre[`@d-botao/${pkg}`] = join(__dirname, 'packages', pkg, 'src');
  return {
    ...pre,
  };
}, {});

// console.log(`🌼 alias list \n${chalk.blue(Object.keys(alias).join('\n'))}`);

const tailPkgList = pkgList
  .map((path) => [join('packages', path, 'src')])
  .reduce((acc, val) => acc.concat(val), []);

const isProduction = process.env.NODE_ENV === 'production';
console.log(alias);

export default defineConfig({
  title: 'botaojs',
  favicon:
    'https://user-images.githubusercontent.com/9554297/83762004-a0761b00-a6a9-11ea-83b4-9c8ff721d4b8.png',
  logo: 'https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg',
  extraBabelPlugins: [
    [
      'import',
      {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
      },
    ],
  ],
  alias,
  resolve: {
    includes: [...tailPkgList, 'docs'],
  },
  externals:
    process.env.NODE_ENV === 'development'
      ? {
          react: 'window.React',
          'react-dom': 'window.ReactDOM',
          moment: 'window.moment',
          // antd: 'window.antd',
        }
      : {},
  targets: {
    chrome: 80,
    firefox: false,
    safari: false,
    edge: false,
    ios: false,
  },
  links:
    process.env.NODE_ENV === 'development'
      ? ['https://gw.alipayobjects.com/os/lib/antd/4.6.6/dist/antd.css']
      : [],
  scripts:
    process.env.NODE_ENV === 'development'
      ? [
          'https://gw.alipayobjects.com/os/lib/react/16.13.1/umd/react.development.js',
          'https://gw.alipayobjects.com/os/lib/react-dom/16.13.1/umd/react-dom.development.js',
          'https://gw.alipayobjects.com/os/lib/moment/2.29.0/min/moment-with-locales.js',
        ]
      : [],
  devServer: {
    port: 8060,
  },
  outputPath: 'docs-dist',
  mode: 'site',
  lessLoader: {
    modifyVars,
  },
  proxy: {
    '/api/': {
      target: 'http://localhost:7001/',
      changeOrigin: true,
      pathRewrite: {
        '^/api': '',
      },
    },
  },
  // more config: https://d.umijs.org/config
});
