import React from 'react';
import { ConfigProvider } from 'antd';

const prefixCls = 'botao-ant';
ConfigProvider.config({
  prefixCls, // 4.13.0+
});

export function rootContainer(container: any) {
  return (
    <ConfigProvider prefixCls={prefixCls} autoInsertSpaceInButton={false}>
      {container}
    </ConfigProvider>
  );
}
