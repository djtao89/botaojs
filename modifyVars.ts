export default {
  '@primary-color': '#2900ff',
  '@botao-prefix': 'botao',
  '@ant-prefix': 'botao-ant',
};
