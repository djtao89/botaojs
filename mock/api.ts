export default {
  // 支持值为 Object 和 Array
  'GET /api/users/get': { users: [1, 2] },

  // 支持自定义函数，API 参考 express@4
  'POST /api/users/post': (req, res) => {
    // 添加跨域请求头
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.json({
      success: true,
      data: [{ id: 'post' }],
      message: 'success',
    });
  },
};
